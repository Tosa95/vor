from time import sleep

from general_utils.files_and_folders.remote_folder_sync import LocalToRemoteFolderSync

ltrfs = LocalToRemoteFolderSync(".", "/home/pi/vor", "192.168.1.33", "pi", "porkanna2!", files_to_avoid=[
    ".git",
    "settings.json",
    "vue_app",
    "venv"
])

try:
    ltrfs.start()
    while True:
        sleep(1)
finally:
    ltrfs.stop()