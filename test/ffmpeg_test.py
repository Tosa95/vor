import eventlet

from general_utils.multimedia.ffmpeg_utils import ffmpeg_limit_video_quality

eventlet.monkey_patch()

import eventlet.green.subprocess
import subprocess

if __name__ == '__main__':
    # sudo ffmpeg -i cf75a389-4014-4b9b-bbc6-f1afd0f8e56c -c:v h264_omx -an prova.mp4
    # process = eventlet.green.subprocess.Popen(
    #     ['ffmpeg', '-i', '820cd692-ec56-4cc5-a484-73a726bc5c64',
    #      '-b:v', '200000',
    #      '-c:v', 'h264_omx',
    #      '-vf', 'scale=320:-2',
    #      '-an',
    #      '-ss', '0',
    #      '-t', '2',
    #      '-y',
    #      "prova.mp4"],
    #     stdout=subprocess.PIPE)
    #
    # stdout = process.communicate()[0]
    # result_code = process.returncode
    # print('STDOUT:{}'.format(result_code))

    ffmpeg_limit_video_quality('test/820cd692-ec56-4cc5-a484-73a726bc5c64', 'test/test.mp4',
                               average_bitrate=200000,
                               video_codec='h264_omx',
                               result_width=320,
                               start=0,
                               end=2,
                               keep_audio=False,
                               subprocess_version=eventlet.green.subprocess)
