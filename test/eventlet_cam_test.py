import eventlet
eventlet.monkey_patch()

from common import base64_encode

import cv2
from camera.cameras.opencv_camera import OpenCVCamera
import time


def on_frame(frame):
    pass
    # cv2.imshow("prova", frame)
    # cv2.waitKey(1)
    # time.sleep(0)
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
    result, frame = cv2.imencode('.jpg', frame, encode_param)
    buffer = frame.tobytes()
    base64 = base64_encode.base64_encode(buffer)
    data_to_send = {
        "frame": "data:image/jpg;base64," + base64.decode('ascii')
    }


if __name__ == '__main__':
    camera = OpenCVCamera(0, 10, (640, 480))
    camera.get_frame_event().add_listener(on_frame)

    time.sleep(100)