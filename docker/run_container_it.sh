#!/bin/sh
docker stop vorc || true
docker rm vorc || true
docker run -it --name vorc \
        -e "REDIS_HOST=redisc" \
        -e "MONGO_HOST=mongodbc" \
        -e "MONGO_USERNAME=voradmin1095" \
        -e "MONGO_PASSWORD=vordbpsw22" \
        -e "RESOURCE_DATA_BASE_DIR=/resdata" \
        -v /opt/vc:/opt/vc \
				-p 80:5000 \
				-v /home/pi/vor:/app \
				-v /dev:/dev \
				-v /home/pi/resdata:/resdata \
				--privileged \
				--restart unless-stopped \
				--network vor-net \
				vor /bin/bash