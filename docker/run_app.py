import eventlet
eventlet.monkey_patch()
from mongoengine import connect

from flask_app import create_app

current_socketio = None

if __name__ == '__main__':

    from db.users import User, USER_TYPE_REAL

    connect("prova", host="mongodbc")

    # Creating superuser if no user is present
    if User.objects().count() == 0:
        try:
            vor = User(username="vor", email="vor@vor.vor", user_type=USER_TYPE_REAL)
            vor.hash_password("vor")
            vor.add_permissions(["*"])
            vor.save()
        except:
            pass

    app, socketio = create_app()
    current_socketio = socketio
    app.debug = True
    socketio.run(app)