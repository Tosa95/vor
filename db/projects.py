import datetime
from copy import deepcopy
from typing import List

from mongoengine import *

from db.users import User, add_permission, del_permission, has_permission
from common.dict_utils import deep_update_dict


class ProjectCollaboratorStatus:
    ACTIVE = 1
    INACTIVE = 0


class Project(Document):
    name = StringField(required=True, unique=True)
    description = StringField()
    owner = ReferenceField(User, reverse_delete_rule=NULLIFY)
    created = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def project_name_exists(cls, name, project_id=None):
        if project_id is None:
            return Project.objects(name=name).count() > 0
        else:
            return Project.objects(name=name, id__ne=project_id).count() > 0


class ProjectCollaborator(Document):
    user = ReferenceField(User, required=True, unique_with="project", reverse_delete_rule=CASCADE)
    project = ReferenceField(Project, required=True, reverse_delete_rule=CASCADE)
    permissions = DictField(default=dict())
    created = DateTimeField(default=datetime.datetime.now)
    status = IntField(default=ProjectCollaboratorStatus.ACTIVE)

    def add_permissions(self, permissions: List[str]):

        # Without this trick it won't work...
        collaborator_perms = deepcopy(self.permissions)

        for perm in permissions:
            add_permission(collaborator_perms, perm)

        self.permissions = collaborator_perms

    def remove_permissions(self, permissions: List[str]):
        collaborator_perms = deepcopy(self.permissions)

        for perm in permissions:
            del_permission(collaborator_perms, perm)

        self.permissions = collaborator_perms

    def get_resource_project_level_permissions(self, res_type):

        if has_permission(self.permissions, "*"):
            return {
                "*": True
            }

        if has_permission(self.permissions, "resource.*"):
            return {
                "resource": {
                    "*": True
                }
            }

        resources_permissions = self.permissions.get("resource", None)

        if resources_permissions is None or not isinstance(resources_permissions,
                                                           dict) or res_type not in resources_permissions:
            return {}

        resource_type_permissions = resources_permissions[res_type]

        if resource_type_permissions is None:
            return {}

        return {
            "resource": resource_type_permissions
        }

    def inherit_permissions_from_user(self, user: User = None):

        if user is None:
            user = User.objects(id=self.user.id).first()

        deep_update_dict(self.permissions, user.get_project_user_level_permissions())
