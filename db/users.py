import datetime
from copy import deepcopy
from typing import List

from mongoengine import *
from passlib.apps import custom_app_context as pwd_context

from common.codes import MAIL_REGEX, PERMISSION_WILDCARD, PERMISSION_SEPARATOR

USER_TYPE_REAL = 0
USER_TYPE_SERVICE = 1


class UserStatus:
    ACTIVE = 1
    INACTIVE = 0


def add_permission(perm_dict, perm: str):
    split = perm.split(PERMISSION_SEPARATOR)

    for i, part in enumerate(split):
        if part not in perm_dict:
            if i + 1 == len(split):
                perm_dict[part] = True
            else:
                perm_dict[part] = {}
        perm_dict = perm_dict[part]


def del_permission(perm_dict, perm: str):
    if not isinstance(perm_dict, dict):
        return

    split = perm.split(PERMISSION_SEPARATOR)

    part = split[0]
    if part in perm_dict:
        if len(split) == 1:
            del perm_dict[part]
        else:
            del_permission(perm_dict[part], PERMISSION_SEPARATOR.join(split[1:]))


def has_permission(perm_dict, perm: str) -> bool:
    split = perm.split(PERMISSION_SEPARATOR)

    for i, part in enumerate(split):
        if PERMISSION_WILDCARD in perm_dict:
            return True
        elif part in perm_dict:
            if i + 1 == len(split):
                return True
            else:
                perm_dict = perm_dict[part]
                if not isinstance(perm_dict, dict):
                    return False
        else:
            return False


class User(Document):
    username = StringField(max_length=100, required=True, unique=True, regex="[A-Za-z0-9_]")
    email = StringField(required=False, regex=MAIL_REGEX)
    password_hash = StringField(required=True, max_length=128)
    date_added = DateTimeField(required=True, default=datetime.datetime.now)
    password_id = IntField(required=True, default=0)
    permissions = DictField(default=dict())
    user_type = IntField(required=False, default=USER_TYPE_REAL)
    status = IntField(required=False, default=UserStatus.ACTIVE)
    lastly_selected_project = ObjectIdField(required=False)

    # Unique index on email that allows multiple null values (for service accounts)
    meta = {"indexes": [
        {"fields": ["email", ],
         "unique": True,
         "partialFilterExpression": {
             "email": {"$type": "string"}
         }
         },
    ]}

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def has_permissions(self, permissions: List[str]):
        return all(has_permission(self.permissions, perm) for perm in permissions)

    def has_permissions_on_project(self, project, permissions: List[str]):
        from db.projects import ProjectCollaborator
        project_user = ProjectCollaborator.objects(project=project, user=self)

        if len(project_user) == 0:
            return False

        project_user = project_user[0]

        return all(has_permission(project_user.permissions, perm) for perm in permissions)

    def add_permissions(self, permissions: List[str]):

        # Without this trick it won't work...
        user_perms = deepcopy(self.permissions)

        for perm in permissions:
            add_permission(user_perms, perm)

        self.permissions = user_perms

    def remove_permissions(self, permissions: List[str]):
        user_perms = deepcopy(self.permissions)

        for perm in permissions:
            del_permission(user_perms, perm)

        self.permissions = user_perms

    def update_permissions(self, permissions: List[str]):
        user_perms = {}

        for perm in permissions:
            user_perms[perm] = True

        self.permissions = user_perms

    @classmethod
    def username_exists(cls, username, user_id=None):
        if user_id is None:
            return User.objects(username=username).count() > 0
        else:
            return User.objects(username=username, id__ne=user_id).count() > 0

    @classmethod
    def email_exists(cls, email, user_id=None):

        if user_id is None:
            return User.objects(email=email).count() > 0
        else:
            return User.objects(email=email, id__ne=user_id).count() > 0

    def get_project_user_level_permissions(self):

        if has_permission(self.permissions, "*"):
            return {
                "*": True
            }

        project_permissions = self.permissions.get("project", None)

        if project_permissions is None:
            return {}

        return {
            "project": project_permissions
        }

    def __repr__(self):
        return self.username

    def __str__(self):
        return self.__repr__()
