from pprint import pprint
from unittest import TestCase

from db.users import add_permission, has_permission, del_permission


class Test_permissions(TestCase):

    def test_add_permission(self):
        permissions = {}

        add_permission(permissions, "test")

        self.assertEqual(permissions, {"test": True})

        add_permission(permissions, "datasets.traffic_sign.contribute")

        self.assertEqual(permissions, {"test": True, "datasets": {
            "traffic_sign": {
                "contribute": True
            }
        }})

        add_permission(permissions, "datasets.traffic_sign.modify")

        self.assertEqual(permissions, {"test": True, "datasets": {
            "traffic_sign": {
                "contribute": True,
                "modify": True
            }
        }})

        pprint(permissions)

    def test_has_permission(self):
        permissions = {
            "test": True,
            "datasets": {
                "traffic_sign": {
                    "contribute": True,
                    "modify": True
                },
                "pose_estimation": {
                    "*": True
                }
            }
        }

        self.assertTrue(has_permission(permissions, "test"))
        self.assertFalse(has_permission(permissions, "test.prova"))
        self.assertTrue(has_permission(permissions, "datasets.traffic_sign"))
        self.assertTrue(has_permission(permissions, "datasets.traffic_sign.contribute"))
        self.assertFalse(has_permission(permissions, "datasets.traffic_sign.contribute.review"))
        self.assertTrue(has_permission(permissions, "datasets.pose_estimation.contribute"))
        self.assertTrue(has_permission(permissions, "datasets.pose_estimation.contribute.review"))

    def test_del_permission(self):
        permissions = {
            "test": True,
            "datasets": {
                "traffic_sign": {
                    "contribute": True,
                    "modify": True
                },
                "pose_estimation": {
                    "*": True
                }
            }
        }

        del_permission(permissions, "test")

        self.assertEqual(permissions, {
            "datasets": {
                "traffic_sign": {
                    "contribute": True,
                    "modify": True
                },
                "pose_estimation": {
                    "*": True
                }
            }
        })

        del_permission(permissions, "datasets.traffic_sign.modify")

        self.assertEqual(permissions, {
            "datasets": {
                "traffic_sign": {
                    "contribute": True
                },
                "pose_estimation": {
                    "*": True
                }
            }
        })

        del_permission(permissions, "datasets.traffic_sign.modify")

        self.assertEqual(permissions, {
            "datasets": {
                "traffic_sign": {
                    "contribute": True
                },
                "pose_estimation": {
                    "*": True
                }
            }
        })

        del_permission(permissions, "datasets.traffic_sign")

        self.assertEqual(permissions, {
            "datasets": {
                "pose_estimation": {
                    "*": True
                }
            }
        })

        del_permission(permissions, "datasets.pose_estimation.*")

        self.assertEqual(permissions, {
            "datasets": {
                "pose_estimation": {
                }
            }
        })

        del_permission(permissions, "datasets")

        self.assertEqual(permissions, {})
