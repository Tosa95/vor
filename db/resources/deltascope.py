from mongoengine import StringField, IntField, FloatField, DictField, ReferenceField, NULLIFY, EmbeddedDocumentField, \
    EmbeddedDocument, ListField

from common.camera.camera_data import CameraData
from common.image_operations.movement_operation import DIFF_ATTENTION_MODE_POLY
from db.resources.base import Resource, ResourceNames
from db.resources.camera import CameraSettings
from db.resources.dataset import DatasetResource


def default_point():
    return [None, None, None]


class SearchArea(EmbeddedDocument):
    p1 = ListField(default=default_point)
    p2 = ListField(default=default_point)
    p3 = ListField(default=default_point)

    center = ListField(default=default_point)
    radius = FloatField(default=None)

class DeltascopeResource(Resource):
    restype = StringField(required=True, default=ResourceNames.DELTASCOPE)

    # Marlin
    com_port = StringField(required=True)
    marlin_manager_name = StringField(required=True)
    baud_rate = IntField(required=True)
    max_radius = FloatField()
    min_z = FloatField()
    max_z = FloatField()
    max_acc = FloatField()
    speed = FloatField()
    positions = DictField()

    # Camera
    camera_settings = EmbeddedDocumentField(CameraSettings, default=CameraSettings())

    # Search
    search_area = EmbeddedDocumentField(SearchArea, default=SearchArea())

    # Data
    experiment = StringField(default="")
    global_dataset = ReferenceField(DatasetResource, reverse_delete_rule=NULLIFY)
    local_dataset = ReferenceField(DatasetResource, reverse_delete_rule=NULLIFY)
