import datetime
from copy import deepcopy
from typing import List

from mongoengine import *

from db.projects import Project, ProjectCollaborator
from db.users import User, add_permission, del_permission
from common.dict_utils import deep_update_dict


class ResourceNames:
    DELTASCOPE = "deltascope"
    DATASET = "dataset"


class ResourceCollaboratorStatus:
    ACTIVE = 1
    INACTIVE = 0


class Resource(Document):
    restype = StringField(required=True)
    name = StringField(required=True)
    description = StringField()
    owner = ReferenceField(User, reverse_delete_rule=NULLIFY)
    created = DateTimeField(default=datetime.datetime.now)
    project = ReferenceField(Project, required=True, reverse_delete_rule=CASCADE)

    meta = {
        'allow_inheritance': True,
        'indexes': [
            {
                'fields': ['name', 'project'],
                'name': 'nameprojectunique',
                'unique': True
            }
        ]
    }

    def resource_name_exists(self, name, resource_id=None):
        if resource_id is None:
            return Resource.objects(name=name, project=self.project).count() > 0
        else:
            return Resource.objects(name=name, project=self.project, id__ne=resource_id).count() > 0


class ResourceCollaborator(Document):
    user = ReferenceField(User, required=True, unique_with=["resource", "project"], reverse_delete_rule=CASCADE)
    resource = ReferenceField(Resource, required=True, reverse_delete_rule=CASCADE)
    project = ReferenceField(Project, required=True, reverse_delete_rule=CASCADE)
    permissions = DictField(default=dict())
    created = DateTimeField(default=datetime.datetime.now)
    status = IntField(default=ResourceCollaboratorStatus.ACTIVE)

    def add_permissions(self, permissions: List[str]):

        # Without this trick it won't work...
        collaborator_perms = deepcopy(self.permissions)

        for perm in permissions:
            add_permission(collaborator_perms, perm)

        self.permissions = collaborator_perms

    def remove_permissions(self, permissions: List[str]):
        collaborator_perms = deepcopy(self.permissions)

        for perm in permissions:
            del_permission(collaborator_perms, perm)

        self.permissions = collaborator_perms

    def inherit_permissions_from_project(self, project_collaborator: ProjectCollaborator = None):

        if project_collaborator is None:
            project_collaborator = ProjectCollaborator.objects(project=self.project.id, user=self.user.id).first()

        # TODO: this if is not completely correct. If the project collaborator is none, it could be that the user has some
        # permissions to inherit from the user record itself into the resource... However it is a problem only for visualization
        if project_collaborator is not None:
            project_collaborator.inherit_permissions_from_user()
            deep_update_dict(self.permissions,
                             project_collaborator.get_resource_project_level_permissions(self.resource.restype))
