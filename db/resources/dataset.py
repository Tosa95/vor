import datetime

from mongoengine import StringField, ReferenceField, CASCADE, DateTimeField, Document, DictField, IntField, \
    BooleanField, EmbeddedDocument, EmbeddedDocumentField, GenericEmbeddedDocumentField

from db.resources.base import Resource, ResourceNames


class DatasetElementTypes:
    TEXT = "text"
    IMAGE = "image"
    AUDIO = "audio"
    VIDEO = "video"


class DatasetResource(Resource):
    restype = StringField(required=True, default=ResourceNames.DATASET)

    element_type = StringField(required=True)

    standard_transform_params = GenericEmbeddedDocumentField()

    preview_transform_params = GenericEmbeddedDocumentField()


class VideoDatasetTransform(EmbeddedDocument):
    average_bitrate = IntField(default=200000)
    result_width = IntField(default=320)
    start = IntField(default=-1)
    end = IntField(default=-1)
    keep_audio = BooleanField(default=True)


class VideoDatasetResource(DatasetResource):
    standard_transform_params = EmbeddedDocumentField(VideoDatasetTransform,
                                                      default=VideoDatasetTransform(
                                                          average_bitrate=2000000,
                                                          result_width=640
                                                      ))

    preview_transform_params = EmbeddedDocumentField(VideoDatasetTransform,
                                                     default=VideoDatasetTransform(
                                                         average_bitrate=200000,
                                                         result_width=320,
                                                         end=2,
                                                         keep_audio=False
                                                     ))


class TextDatasetTransform(EmbeddedDocument):
    max_length = IntField(default=10000000)


class TextDatasetResource(DatasetResource):
    standard_transform_params = EmbeddedDocumentField(TextDatasetTransform,
                                                      default=TextDatasetTransform(max_length=10000000))

    preview_transform_params = EmbeddedDocumentField(TextDatasetTransform,
                                                     default=TextDatasetTransform(max_length=300))


class ImageDatasetTransform(EmbeddedDocument):
    max_size = IntField(default=1920)


class ImageDatasetResource(DatasetResource):
    standard_transform_params = EmbeddedDocumentField(ImageDatasetTransform,
                                                      default=ImageDatasetTransform(max_size=1920))

    preview_transform_params = EmbeddedDocumentField(ImageDatasetTransform,
                                                     default=ImageDatasetTransform(max_size=320))


class DatasetElement(Document):
    dataset = ReferenceField(DatasetResource, reverse_delete_rule=CASCADE)

    data_path = StringField()
    data_name = StringField()

    data_format = StringField()

    metadata = DictField()

    # Should be the instant in which the element was acquired (e.g. a picture was shot).
    # Try to set it correctly if possible
    created = DateTimeField(default=datetime.datetime.now)
    # The time in which the element was added to the database (it is ok to leave the default value)
    added = DateTimeField(default=datetime.datetime.now)
