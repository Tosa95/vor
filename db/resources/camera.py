from mongoengine import EmbeddedDocument, StringField, FloatField, IntField, BooleanField

from common.camera.camera_data import CameraData
from common.image_operations.movement_operation import DIFF_ATTENTION_MODE_POLY


class CameraSettings(EmbeddedDocument):
    # Camera
    camera_name = StringField(required=True)
    min_zoom_factor = FloatField(default=1)
    max_zoom_factor = FloatField(default=5)
    min_quality = FloatField(default=80)
    max_quality = FloatField(default=95)
    accepted_delay = FloatField(default=1)
    target_fps_factor = FloatField(default=0.9)
    zoom_factor = FloatField(default=1.0)

    # Movement
    diff_enabled = BooleanField(default=True)
    diff_resize_factor = FloatField(default=2.0)
    diff_threshold = FloatField(default=2.0)
    diff_time_dist = IntField(default=2)
    diff_closing_kernel_size = IntField(default=25)
    diff_opening_kernel_size = IntField(default=3)
    diff_contour_length_threshold = IntField(default=5)
    diff_denoise_filter_size = IntField(default=3)
    diff_attentive_factor = FloatField(default=0.8)
    diff_attention_mode = StringField(default=DIFF_ATTENTION_MODE_POLY)
    diff_attention_persistence = FloatField(default=0.95)

    def get_camera_data(self) -> CameraData:
        return CameraData(
            self.camera_name,
            self.min_zoom_factor,
            self.max_zoom_factor,
            self.min_quality,
            self.max_quality,
            self.accepted_delay,
            self.target_fps_factor,
            self.zoom_factor,
            self.diff_enabled,
            self.diff_resize_factor,
            self.diff_threshold,
            self.diff_time_dist,
            self.diff_closing_kernel_size,
            self.diff_opening_kernel_size,
            self.diff_contour_length_threshold,
            self.diff_denoise_filter_size,
            self.diff_attentive_factor,
            self.diff_attention_mode,
            self.diff_attention_persistence
        )
