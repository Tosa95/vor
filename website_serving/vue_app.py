from flask import Blueprint, url_for

vue_app_bp = Blueprint("vue_app", __name__, url_prefix="/app", static_url_path='/',
                       static_folder='./app')
