import cv2

from common import base64_encode

cap = cv2.VideoCapture(1)

encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

while(True):
    # Capture frame-by-frame

    ret, frame = cap.read()
    result, frame = cv2.imencode('.jpg', frame, encode_param)
    bytes = frame.tobytes()
    base64 = base64_encode.base64_encode(bytes)
    print(len(bytes), len(base64))
    frame = cv2.imdecode(frame, 1)

    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

ret, frame = cap.read()
print(type(frame), frame.shape)
result, frame = cv2.imencode('.jpg', frame, encode_param)
print(type(frame), frame.shape)
frame = cv2.imdecode(frame, 1)
print(type(frame), frame.shape)

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()