import eventlet
eventlet.monkey_patch()

import time

import cv2

from camera.cameras.opencv_camera import OpenCVCamera

if __name__ == "__main__":

    camera = OpenCVCamera(0, fps=20)

    def on_frame(frame):
        print(frame.shape)
        cv2.imshow('frame', frame)
        cv2.waitKey(1)

    camera.get_frame_event().add_listener(on_frame)

    time.sleep(5)
    #
    # camera.stop()