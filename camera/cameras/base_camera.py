from abc import ABC

from common.event import Event



class BaseCamera(ABC):

    def __init__(self):
        self._frame_event = Event()

    def get_frame_event(self) -> Event:
        return self._frame_event