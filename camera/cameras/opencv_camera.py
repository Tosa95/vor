import eventlet
eventlet.monkey_patch()

import time
from datetime import date, datetime
from queue import Full, Empty, Queue
from threading import Thread
from camera.cameras.base_camera import BaseCamera
import cv2
# import eventlet
#
# cv2 = eventlet.import_patched('cv2')
# multiprocessing = eventlet.import_patched('multiprocessing')
# Queue = multiprocessing.Queue
# Process = multiprocessing.Process


class OpenCVCamera(BaseCamera):

    def __init__(self, index, fps=15.0, resolution=(640, 480), max_enqueued_frames=2, queue_timeout=0.1):
        super().__init__()
        self._index = index
        self._fps = fps
        self._resolution = resolution
        self._period = 1.0 / fps
        self._stop = False
        self._queue_timeout = queue_timeout
        self._capture_queue = Queue(maxsize=max_enqueued_frames)
        # self._capturing_thread = Thread(target=self._cycle)
        self._capturing_thread = Thread(target=self._cycle)
        self._capturing_thread.start()
        self._firing_thread = Thread(target=self._fire)
        self._firing_thread.start()

    def _cycle(self):

        cap = cv2.VideoCapture()

        cap.open(self._index, cv2.CAP_ANY)

        cap.set(3, self._resolution[0])
        cap.set(4, self._resolution[1])

        while not self._stop:
            # Capture frame-by-frame
            frame_begin_time = datetime.now()
            ret, frame = cap.read()
            try:
                self._capture_queue.put(frame, block=True, timeout=self._queue_timeout)
            except Full:
                pass
            frame_end_time = datetime.now()
            elapsed = (frame_end_time - frame_begin_time).total_seconds()
            print(elapsed)
            to_wait = max(0.01, self._period - elapsed)
            # print(to_wait)
            time.sleep(to_wait)

    def _fire(self):
        pass
        while not self._stop:
            try:
                frame = self._capture_queue.get(block=True, timeout=self._queue_timeout)
                self._frame_event.fire(frame)
            except Empty:
                pass
            time.sleep(0)

    def stop(self):
        self._stop = True
        self._capturing_thread.join()
        self._firing_thread.join()
