from flask import Blueprint, jsonify, current_app, url_for, make_response
from schema import Optional

from common.decorators.decorators import validate_request
from common.decorators.func_data_map import FUNC_DATA_MAP
from rest_api.connector_generators import CONNECTOR_GENERATOR

documentation_bp = Blueprint("documentation", __name__, url_prefix="/api/documentation")

def schema_to_serializable_dict(schema):
    res = {}

    for pname, ptype in schema.items():
        optional = False
        if issubclass(type(pname), Optional):
            pname = str(pname).split("'")[1]
            optional = True

        res[pname] = {
            "optional": optional,
            "type": str(ptype)
        }

    return res

def get_routes_data(root=None):
    routes_data = []
    for rule in current_app.url_map.iter_rules():
        url = None

        try:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
        except:
            pass

        if root is None or (url is not None and url.startswith(root)):

            methods = [m for m in rule.methods if m != "HEAD" and m != "OPTIONS"]
            endpoint = rule.endpoint
            endpoint_split = endpoint.split(".")
            func_name = None

            if len(endpoint_split) > 1:
                func_name = endpoint_split[1]

            routes_data.append({
                "url": url,
                "endpoint": endpoint,
                "func_name": func_name,
                "methods": methods,
                "requires_login": FUNC_DATA_MAP.get_function_requires_login(func_name),
                "requires_project": FUNC_DATA_MAP.get_function_requires_project(func_name),
                "requires_resource": FUNC_DATA_MAP.get_function_requires_resource(func_name),
                "user_level_permissions": FUNC_DATA_MAP.get_function_user_level_permissions(func_name),
                "project_level_permissions": FUNC_DATA_MAP.get_function_project_level_permissions(func_name),
                "resource_level_permissions": FUNC_DATA_MAP.get_function_resource_level_permissions(func_name),
                "schema": schema_to_serializable_dict(FUNC_DATA_MAP.get_function_schema(func_name))
            })

    return routes_data


@documentation_bp.route("/route", methods=["GET"])
@validate_request(schema={
    Optional("root"): str
})
def documentation_route(data):
    root = data.get("root", None)

    return jsonify({"routes_data": get_routes_data(root)}), 200


@documentation_bp.route("/connector", methods=["GET"])
@validate_request(schema={
    Optional("language"): str,
    Optional("root"): str
})
def documentation_connector(data):
    language = data.get("language", "javascript")
    root = data.get("root", None)

    response = make_response(CONNECTOR_GENERATOR.get_connector(root, get_routes_data(root), language), 200)
    response.mimetype = "text/plain"

    return response
