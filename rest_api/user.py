import secrets
import urllib
from urllib.parse import urlencode

from flask import Blueprint, json, current_app as app, jsonify
from flask_mail import Message, Mail
from itsdangerous import TimedSerializer
from mongoengine import ValidationError
from schema import Or, Optional

from common.token.default_token_manager import DefaultTokenManager
from db.users import User, USER_TYPE_REAL, USER_TYPE_SERVICE, UserStatus
from common.codes import TokenType
from common.decorators.decorators import validate_request, mongo_query_validate
from common.decorators.requirements import requires_login
from common.decorators.permissions import has_permissions
from common.errors import exception_json, ErrorID
from common.mongo import mongo_query

user_bp = Blueprint("user", __name__, url_prefix="/api/user")


@user_bp.route("/register", methods=["POST"])
@validate_request(schema={
    "username": str,
    "email": str,
    "password": str
})
def user_register(data):
    user = User(username=data["username"], email=data["email"], user_type=USER_TYPE_REAL)
    user.hash_password(data["password"])

    try:
        user.validate()
    except ValidationError as e:
        return exception_json(e)

    if User.username_exists(data["username"]):
        return exception_json(Exception("Username already present."), error_id=ErrorID.USERNAME_ALREADY_PRESENT)

    if User.email_exists(data["email"]):
        return exception_json(Exception("Email already present."), error_id=ErrorID.EMAIL_ALREADY_PRESENT)

    try:
        user.save()
    except Exception as e:
        return exception_json(e)

    return json.jsonify({"msg": "User inserted."}), 201


@user_bp.route("/forgot_password", methods=["GET", "POST"])
@validate_request(schema={
    "username": str
})
def user_forgot_password(data):
    user = User.objects(username=data["username"])

    if len(user) == 0:
        return exception_json(Exception("User not found."), code=404, error_id=ErrorID.USER_NOT_FOUND)

    user = user[0]

    tm = DefaultTokenManager.get()
    token = tm.sign(
        {'id': str(user.id),
         'password_id': str(user.password_id),
         "token_type": TokenType.PASSWORD_RESET},
        expiration=TokenType.get_token_type_expiry_time(TokenType.PASSWORD_RESET))

    mail = Mail(app)
    msg = Message(subject="Vor password reset",
                  sender=app.config.get("MAIL_USERNAME"),
                  recipients=[user.email],
                  html="Click on the following link to reset your password.<br/><a href=\"%s?token=%s\">Reset your password</a>" % (
                      app.config["RESET_PASSWORD_BASE_URL"], urllib.parse.quote(token)))
    mail.send(msg)
    return jsonify({"msg": "Password reset email sent."}), 200


@user_bp.route("/forgot_password/update", methods=["PATCH"])
@validate_request(schema={"new_password": str})
@requires_login(accepted_token_type=TokenType.PASSWORD_RESET)
def user_forgot_password_update(data: dict, user):
    user.hash_password(data["new_password"])
    user.password_id += 1
    user.save()
    return jsonify({"msg": "Password updated."}), 200


@user_bp.route("/service_account/create", methods=["POST"])
@validate_request(schema={
    "username": str
})
def user_service_account_create(data):
    username = data["username"]
    password = secrets.token_hex(app.config["SERVICE_ACCOUNT_PASSWORD_LENGTH"])

    user = User(username=username, user_type=USER_TYPE_SERVICE)
    user.hash_password(password)

    try:
        user.validate()
    except ValidationError as e:
        return json.jsonify({"msg": str(e)}), 400

    if User.username_exists(data["username"]):
        return exception_json(Exception("Username already present."), error_id=ErrorID.USERNAME_ALREADY_PRESENT)

    try:
        user.save()
    except Exception as e:
        return exception_json(e)

    return json.jsonify({"msg": "User inserted.", "credentials": {"username": username, "password": password}}), 201


@user_bp.route("/login", methods=["GET", "POST"])
@validate_request(
    schema={
        Optional("username"): str,
        Optional("email"): str,
        "password": str}
)
def user_login(data):
    user = None
    if "email" in data:
        user = User.objects(email=data["email"])
    elif "username" in data:
        user = User.objects(username=data["username"])

    if len(user) == 0:
        return exception_json(Exception("User not found."), 404, error_id=ErrorID.USER_NOT_FOUND)

    user = user[0]

    if user.status == UserStatus.INACTIVE:
        return exception_json(Exception("Account was deactivated."), 401, error_id=ErrorID.ACCOUNT_DEACTIVATED)

    if user.verify_password(data["password"]):
        tm = DefaultTokenManager.get()
        token = tm.sign(
            {'id': str(user.id),
             'password_id': str(user.password_id),
             "token_type": TokenType.STANDARD},
            expiration=TokenType.get_token_type_expiry_time(TokenType.STANDARD))
        return json.jsonify({"_token": token}), 200
    else:
        return exception_json(Exception("Password does not match"), 401, error_id=ErrorID.WRONG_PASSWORD)


@user_bp.route("/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
@has_permissions(["user.get"])
@mongo_query_validate(force_exclude=["password_hash"])
def user_get(data: dict, user):
    return mongo_query(User, data)


@user_bp.route("/del", methods=["DELETE"])
@validate_request(schema={"id": str})
@requires_login()
@has_permissions(["user.delete"])
def user_del(data: dict, user):
    user = User.objects(id=data["id"]).first()

    if user is None:
        return exception_json(Exception("User not found."), 404, error_id=ErrorID.USER_NOT_FOUND)

    user.delete()

    return jsonify({"msg": "User deleted."}), 200


@user_bp.route("/status/set", methods=["PATCH"])
@validate_request(schema={"id": str, "new_status": int})
@requires_login()
@has_permissions(["user.status.set"])
def user_status_set(data: dict, user):
    user = User.objects(id=data["id"])

    if user.count() == 0:
        return exception_json(Exception("User not found."), 404, error_id=ErrorID.USER_NOT_FOUND)

    user = user[0]

    user.status = data["new_status"]
    user.save()

    return jsonify({"msg": "User status updated."}), 200


@user_bp.route("/info/update", methods=["PATCH"])
@validate_request(schema={
    "id": str,
    Optional("new_username"): str,
    Optional("new_email"): str
})
@requires_login()
@has_permissions(["user.modify"])
def user_info_update(data: dict, user):
    user = User.objects(id=data["id"])

    if user.count() == 0:
        return exception_json(Exception("User not found."), 404, error_id=ErrorID.USER_NOT_FOUND)

    user = user[0]

    if "new_email" in data:
        user.email = data["new_email"]
        if User.email_exists(data["new_email"], user_id=user.id):
            return exception_json(Exception("Email already present."), error_id=ErrorID.EMAIL_ALREADY_PRESENT)

    if "new_username" in data:
        user.username = data["new_username"]
        if User.username_exists(data["new_username"], user_id=user.id):
            return exception_json(Exception("Username already present."), error_id=ErrorID.USERNAME_ALREADY_PRESENT)

    try:
        user.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "User info modified."}), 200


@user_bp.route("/password/update", methods=["PATCH"])
@validate_request(schema={
    "id": str,
    "new_password": str
})
@requires_login()
@has_permissions(["user.password.modify"])
def user_password_update(data: dict, user):
    user = User.objects(id=data["id"])

    if user.count() == 0:
        return exception_json(Exception("User not found."), 404, error_id=ErrorID.USER_NOT_FOUND)

    user = user[0]

    user.hash_password(data["new_password"])

    try:
        user.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "User password modified."}), 200


@user_bp.route("/permission/add", methods=["PATCH"])
@validate_request(schema={
    "id": str,
    "permissions": list
})
@requires_login()
@has_permissions(["user.modify"])
def user_permission_add(data: dict, user):
    user = User.objects(id=data["id"]).first()
    if user is None:
        return exception_json(Exception("User not found"), code=404, error_id=ErrorID.USER_NOT_FOUND)

    user.add_permissions(data["permissions"])

    user.save()

    return jsonify({"msg": "Permissions modified."}), 200


@user_bp.route("/permission/del", methods=["DELETE"])
@validate_request(schema={
    "id": str,
    "permissions": list
})
@requires_login()
@has_permissions(["user.modify"])
def user_permission_del(data: dict, user):
    user = User.objects(id=data["id"]).first()
    if user_bp is None:
        return exception_json(Exception("User not found"), code=404, error_id=ErrorID.USER_NOT_FOUND)

    user.remove_permissions(data["permissions"])

    user.save()

    return jsonify({"msg": "Permissions removed."}), 200


@user_bp.route("/permission/update", methods=["PATCH"])
@validate_request(schema={
    "id": str,
    "permissions": list
})
@requires_login()
@has_permissions(["user.modify"])
def user_permission_update(data: dict, user):
    user = User.objects(id=data["id"])
    if user is None:
        return exception_json(Exception("User not found"), code=404, error_id=ErrorID.USER_NOT_FOUND)

    user.update_permissions(data["permissions"])

    user.save()

    return jsonify({"msg": "Permissions removed."}), 200


@user_bp.route("/self/info/update", methods=["PATCH"])
@validate_request(schema={
    Optional("email"): str,
    Optional("username"): str
})
@requires_login()
def user_self_info_update(data: dict, user):
    if "email" in data:
        original_email = user.email
        user.email = data["email"]
        if data["email"] != original_email and User.email_exists(data["email"]):
            return exception_json(Exception("Email already present."), error_id=ErrorID.EMAIL_ALREADY_PRESENT)

    if "username" in data:
        original_username = user.username
        user.username = data["username"]
        if data["username"] != original_username and User.username_exists(data["username"]):
            return exception_json(Exception("Username already present."), error_id=ErrorID.USERNAME_ALREADY_PRESENT)

    try:
        user.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "User info modified."}), 200


@user_bp.route("/self/info/get", methods=["GET"])
@validate_request(schema={
})
@requires_login()
def user_self_info_get(data: dict, user):
    user_data = json.loads(user.to_json())
    del user_data["password_hash"]
    return jsonify({"info": user_data}), 200


@user_bp.route("/self/password/update", methods=["PATCH"])
@validate_request(schema={
    "old_password": str,
    "new_password": str
})
@requires_login()
def user_self_password_update(data: dict, user):
    if user.verify_password(data["old_password"]):
        user.hash_password(data["new_password"])
        user.password_id += 1
        user.save()
        return jsonify({"msg": "User password modified."}), 200

    return exception_json(Exception("Old password does not match"), code=401, error_id=ErrorID.WRONG_PASSWORD)


@user_bp.route("/self/token/test", methods=["GET", "POST"])
@validate_request(schema={
})
@requires_login()
def user_self_token_test(data: dict, user):
    return jsonify({"msg": "Token ok."}), 200


@user_bp.route("/username_used", methods=["GET", "POST"])
@validate_request(schema={
    Optional("user_id"): str,
    "username": str
})
def user_username_used(data: dict):
    user_id = data.get("user_id", None)
    return jsonify({"exists": User.username_exists(data["username"], user_id)}), 200


@user_bp.route("/email_used", methods=["GET", "POST"])
@validate_request(schema={
    Optional("user_id"): str,
    "email": str
})
def user_email_used(data: dict):
    user_id = data.get("user_id", None)
    return jsonify({"exists": User.email_exists(data["email"], user_id)}), 200
