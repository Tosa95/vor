from flask import Blueprint, jsonify
from schema import Optional

from db.projects import Project, ProjectCollaborator, ProjectCollaboratorStatus
from db.users import User
from common.codes import PERMISSION_WILDCARD
from common.decorators.decorators import validate_request, mongo_query_validate
from common.decorators.requirements import requires_login, requires_project
from common.decorators.permissions import has_permissions_on_project, has_permissions
from common.errors import exception_json, ErrorID
from common.mongo import mongo_query_result_to_json, list_to_flask_response, \
    list_query

import db.users

project_bp = Blueprint("project", __name__, url_prefix="/api/project")


def project_collaborator_to_project(project_user: ProjectCollaborator):
    permissions = project_user.permissions
    project = project_user.project

    res = {
        "permissions": permissions
    }

    res.update(mongo_query_result_to_json(project))

    return res


def project_collaborator_to_user(project_collaborator: ProjectCollaborator):
    permissions = project_collaborator.permissions
    status = project_collaborator.status
    user = project_collaborator.user

    res = mongo_query_result_to_json(user)

    res.update({
        "permissions": permissions,
        "status": status
    })

    return res


@project_bp.route("/add", methods=["POST"])
@validate_request(schema={"name": str, Optional("description"): str})
@requires_login()
@has_permissions(["project.add"])
def project_add(data: dict, user):
    description = data.get("description", None)
    project = Project(name=data["name"], description=description, owner=user)

    if Project.project_name_exists(data["name"]):
        return exception_json(Exception("Project name already taken."), error_id=ErrorID.PROJECT_NAME_ALREADY_PRESENT)

    try:
        project.save()
        project_user = ProjectCollaborator(project=project, user=user,
                                           permissions={
                                               "project": {PERMISSION_WILDCARD: True}
                                           })
        project_user.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Project added."}), 201


@project_bp.route("/del", methods=["DELETE"])
@validate_request()
@requires_login()
@requires_project()
@has_permissions_on_project(["project.del"])
def project_del(data: dict, user: User, project_collaborator: ProjectCollaborator):
    project_collaborator.project.delete()
    return jsonify({"msg": "Project deleted."}), 200


@project_bp.route("/self/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
@mongo_query_validate()
def project_self_get(data: dict, user: User):
    project_collaborators = list(ProjectCollaborator.objects(user=user.id))

    # Checking for non collaborating projects that user should see anyway
    if db.users.has_permission(user.permissions, "project.get"):
        collaborating_projects_ids = set()

        for collab in project_collaborators:
            collaborating_projects_ids.add(collab.project.id)

        projects = Project.objects()

        for project in projects:
            if project.id not in collaborating_projects_ids:
                project_collaborators.append(ProjectCollaborator(user=user.id, project=project.id))

    for collab in project_collaborators:
        collab.inherit_permissions_from_user(user)

    projects = [project_collaborator_to_project(pu) for pu in project_collaborators]

    selected_projects, count = list_query(projects, data)

    return list_to_flask_response(selected_projects, count)


@project_bp.route("/self/lastly_selected/set", methods=["PATCH"])
@validate_request()
@requires_login()
@requires_project()
def project_self_lastly_selected_set(data: dict, user: User, project_user: ProjectCollaborator):
    user.lastly_selected_project = project_user.project.id
    user.save()
    return jsonify({"msg": "Lastly selected project set."}), 200


@project_bp.route("/self/lastly_selected/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
def project_self_lastly_selected_get(data: dict, user: User):
    lastly_selected_project = ProjectCollaborator.objects(user=user.id, project=user.lastly_selected_project).first()

    if lastly_selected_project is not None:
        lastly_selected_project.inherit_permissions_from_user(user)
        lastly_selected_project = project_collaborator_to_project(lastly_selected_project)
        return jsonify({"lastly_selected_project": lastly_selected_project}), 200
    else:
        project = Project.objects(id=user.lastly_selected_project).first()
        if project is not None and db.users.has_permission(user.permissions, "project.get"):
            lastly_selected_project = ProjectCollaborator(user=user.id, project=project.id)
            lastly_selected_project.inherit_permissions_from_user()
            lastly_selected_project = project_collaborator_to_project(lastly_selected_project)

            return jsonify({"lastly_selected_project": lastly_selected_project}), 200
        else:
            return exception_json(Exception("Project not found."), 404, error_id=ErrorID.PROJECT_NOT_FOUND)


@project_bp.route("/collaborator/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
@requires_project()
@has_permissions_on_project(["project.collaborator.get"])
@mongo_query_validate(force_exclude=["password_hash", "password_id"])
def project_collaborator_get(data: dict, user: User, project_collaborator: ProjectCollaborator):
    project_collaborators = ProjectCollaborator.objects(project=project_collaborator.project.id)

    for collab in project_collaborators:
        collab.inherit_permissions_from_user()

    collaborators = [project_collaborator_to_user(pu) for pu in project_collaborators]

    selected_collaborators, count = list_query(collaborators, data)

    return list_to_flask_response(selected_collaborators, count)


@project_bp.route("/collaborator/add", methods=["POST"])
@validate_request(schema={
    "user_id": str,
    Optional("permissions"): dict,
    Optional("status"): int
})
@requires_login()
@requires_project()
@has_permissions_on_project(["project.collaborator.add"])
def project_collaborator_add(data: dict, user: User, project_collaborator: ProjectCollaborator):
    user_id = data["user_id"]
    permissions = data.get("permissions", None)
    status = data.get("status", ProjectCollaboratorStatus.ACTIVE)

    new_collaborator = ProjectCollaborator(user=user_id, project=project_collaborator.project.id,
                                           permissions=permissions, status=status)

    try:
        new_collaborator.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Collaborator added."}), 200


@project_bp.route("/collaborator/del", methods=["DELETE"])
@validate_request(schema={
    "user_id": str
})
@requires_login()
@requires_project()
@has_permissions_on_project(["project.collaborator.del"])
def project_collaborator_del(data: dict, user: User, project_collaborator: ProjectCollaborator):
    collaborator_to_remove = ProjectCollaborator.objects(user=data["user_id"],
                                                         project=project_collaborator.project.id).first()

    if collaborator_to_remove is None:
        return exception_json(Exception("Collaborator not found"), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_remove.delete()

    return jsonify({"msg": "Collaborator deleted."}), 200


@project_bp.route("/collaborator/permission/add", methods=["POST"])
@validate_request(schema={
    "user_id": str,
    "permissions": list
})
@requires_login()
@requires_project()
@has_permissions_on_project(["project.collaborator.permission.set"])
def project_collaborator_permission_add(data: dict, user: User, project_collaborator: ProjectCollaborator):
    collaborator_to_update = ProjectCollaborator.objects(user=data["user_id"],
                                                         project=project_collaborator.project.id).first()

    if collaborator_to_update is None:
        return exception_json(Exception("Collaborator not found."), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_update.add_permissions(data["permissions"])
    collaborator_to_update.save()

    return jsonify({"msg": "Collaborator permissions added."}), 200


@project_bp.route("/collaborator/permission/del", methods=["DELETE"])
@validate_request(schema={
    "user_id": str,
    "permissions": list
})
@requires_login()
@requires_project()
@has_permissions_on_project(["project.collaborator.permission.set"])
def project_collaborator_permission_del(data: dict, user: User, project_collaborator: ProjectCollaborator):
    collaborator_to_update = ProjectCollaborator.objects(user=data["user_id"],
                                                         project=project_collaborator.project.id).first()

    if collaborator_to_update is None:
        return exception_json(Exception("Collaborator not found"), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_update.remove_permissions(data["permissions"])
    collaborator_to_update.save()

    return jsonify({"msg": "Collaborator permissions removed."}), 200


@project_bp.route("/collaborator/status/set", methods=["PATCH"])
@validate_request(schema={
    "user_id": str,
    "new_status": int
})
@requires_login()
@requires_project()
@has_permissions_on_project(["project.collaborator.status.set"])
def project_collaborator_status_set(data: dict, user: User, project_collaborator: ProjectCollaborator):
    collaborator_to_update = ProjectCollaborator.objects(user=data["user_id"],
                                                         project=project_collaborator.project.id).first()

    if collaborator_to_update is None:
        return exception_json(Exception("Collaborator not found."), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_update.status = data["new_status"]
    collaborator_to_update.save()

    return jsonify({"msg": "Collaborator status set."}), 200


@project_bp.route("/set", methods=["PATCH"])
@validate_request(schema={
    Optional("name"): str,
    Optional("description"): str
})
@requires_login()
@requires_project()
@has_permissions_on_project(["project.set"])
def project_set(data: dict, user: User, project_collaborator: ProjectCollaborator):
    project = project_collaborator.project

    if "name" in data:
        project.name = data["name"]

    if "description" in data:
        project.description = data["description"]

    project.save()

    return jsonify({"msg": "Project set."}), 200
