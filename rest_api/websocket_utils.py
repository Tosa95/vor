from flask import request, current_app
from flask_socketio import Namespace, join_room, leave_room
from common.cache.authorization_cache import AUTHORIZATION_CACHE, AuthorizationCacheEntry

class BaseNamespace(Namespace):

    def __init__(self, namespace):
        super().__init__(namespace=namespace)

        self._context = None

    @staticmethod
    def contextualize():
        def decorator(func):
            def wrapper(self, *args, **kwargs):
                if self._context is not None:
                    with self._context:
                        func(self, *args, **kwargs)

            wrapper.__name__ = func.__name__
            return wrapper

        return decorator

    def on_connect(self):
        print(
            "Someone connected " + request.environ['REMOTE_ADDR'] + " " + request.environ[
                'REMOTE_PORT'] + " " + request.sid)
        self._context = current_app._get_current_object().app_context()
        pass

    def on_disconnect(self):
        AUTHORIZATION_CACHE.remove_session(request.sid)
        print(
            "Someone disconnected " + request.environ['REMOTE_ADDR'] + " " + request.environ[
                'REMOTE_PORT'] + " " + request.sid)
        pass

    def manage_room_join(self, room_name, data, validator):
        namespace = self.namespace

        def expire_callback(cache_entry: AuthorizationCacheEntry):
            with cache_entry.get_context():
                leave_room(room_name, cache_entry.get_sid(), namespace=namespace)
                print("Room left")

        AUTHORIZATION_CACHE.add_authorization(
            room_name,
            AuthorizationCacheEntry(
                request.sid,
                data,
                validator,
                expire_callback,
                context=current_app._get_current_object().app_context(),
                additional_data={
                    "room_name": room_name,
                    "namespace": namespace
                }
            )
        )
        join_room(room_name, namespace=self.namespace)

        return {"msg": "Added to room %s" % room_name, "room_name": room_name}

    def manage_command_authorization(self, command_name, data, validator):
        namespace = self.namespace

        AUTHORIZATION_CACHE.add_authorization(
            command_name,
            AuthorizationCacheEntry(
                request.sid,
                data,
                validator,
                context=current_app._get_current_object().app_context(),
                additional_data={
                    "command_name": command_name,
                    "namespace": namespace
                }
            )
        )

        return {"msg": "Authorized to issue command %s" % command_name, "command_name": command_name}
