from copy import deepcopy
from pprint import pprint
from typing import List
from uuid import uuid4

from bson import ObjectId
from schema import Optional, Or
from serial.tools import list_ports
from flask import Blueprint, jsonify

from camera.cameras.camera_cache import CAMERA_CACHE
from common.base64_encode import base64_encode
from common.marlin.remote_marlin_manager import RemoteMarlinManager
from db.projects import ProjectCollaborator
from db.resources.base import ResourceCollaborator
from db.resources.camera import CameraSettings
from db.resources.dataset import DatasetResource, DatasetElement
from db.resources.deltascope import DeltascopeResource, SearchArea
from db.users import User, has_permission
from common.codes import PERMISSION_WILDCARD
from common.decorators.decorators import validate_request, mongo_query_validate
from common.decorators.requirements import requires_login, requires_project, requires_resource
from common.decorators.permissions import has_permissions_on_project, has_permissions_on_resource
from common.errors import exception_json, ErrorID
from general_utils.multimedia.ffmpeg_utils import ffmpeg_limit_video_quality
from general_utils.multimedia.video_encoding import frames_generator_to_video
from marlin_connector.marlin_cache import MARLIN_CACHE
from marlin_connector.marlin_command_manager import DeltaWorkSpaceCheck
from common.mongo import list_to_flask_response, mongo_document_to_json
from rest_api.resources.data import ResourceDataManager
from rest_api.resources.dataset import direct_dataset_element_add, direct_dataset_element_get, \
    direct_dataset_element_del, direct_dataset_clear
from common.camera.camera_manager import CameraManager
from settings.orchestrator import Orchestrator
from settings.special_paths import RESOURCE_TEMP_PATH

deltascope_bp = Blueprint("deltascope", __name__, url_prefix="/api/resource/deltascope")

DELTASCOPE_DEFAULT_SETTINGS = {
    "min_zoom_factor": 1.0,
    "max_zoom_factor": 10.0,
    "min_quality": 50,
    "max_quality": 100,
    "accepted_delay": 0.010,
    "target_fps_factor": 0.98,
    "baud_rate": 250000,
    "max_radius": 50,
    "min_z": 100,
    "max_z": 250,
    "max_acc": 300,
    "speed": 5000
}

LOCAL_DATASET_REQUIRED_PERMISSIONS = [
    "resource.element.get",
    "resource.element.add",
    "resource.element.del",
    "resource.clear"
]

GLOBAL_DATASET_REQUIRED_PERMISSIONS = [
    "resource.element.add"
]


# TODO: make this thing of checking permissions on sub resources general (decorator?)
def check_permissions_on_dataset(user: User, dataset_id: str, permissions: List[str]):
    resource_collaborator: ResourceCollaborator = ResourceCollaborator.objects(resource=ObjectId(dataset_id),
                                                                               user=user.id).first()

    resource_collaborator.inherit_permissions_from_project()

    for permission in permissions:
        if not has_permission(resource_collaborator.permissions, permission):
            raise Exception("You must have the permission %s on the dataset %s." % (
                permission, resource_collaborator.resource.name))


@deltascope_bp.route("/com_port/get", methods=["GET", "POST"])
@validate_request(schema={
    "marlin_manager_name": str
})
@requires_login()
@requires_project()
@has_permissions_on_project(["resource.deltascope.com_port.get"])
def deltascope_com_port_get(data: dict, user: User, project_collaborator: ProjectCollaborator):
    # res = [com_port.device for com_port in list_ports.comports()]

    ports = RemoteMarlinManager.get_com_ports(Orchestrator.get(), data["marlin_manager_name"])

    return list_to_flask_response(ports, len(ports))


@deltascope_bp.route("/add", methods=["POST"])
@validate_request(schema={
    "name": str,
    "marlin_manager_name": str,
    "com_port": str,
    "camera_name": str,
    "baud_rate": int,
    Optional("description"): str,
    "max_radius": Or(int, float),
    "min_z": Or(int, float),
    "max_z": Or(int, float),
    "max_acc": Or(int, float),
    "speed": Or(int, float),
    "min_zoom_factor": Or(int, float),
    "max_zoom_factor": Or(int, float),
    "min_quality": Or(int, float),
    "max_quality": Or(int, float),
    "accepted_delay": Or(int, float),
    "target_fps_factor": Or(int, float),
    "global_dataset_id": str,
    "local_dataset_id": str
})
@requires_login()
@requires_project()
@has_permissions_on_project(["resource.deltascope.add"])
def deltascope_add(data: dict, user: User, project_collaborator: ProjectCollaborator):
    description = data.get("description", None)

    check_permissions_on_dataset(user, data["global_dataset_id"], GLOBAL_DATASET_REQUIRED_PERMISSIONS)
    check_permissions_on_dataset(user, data["local_dataset_id"], LOCAL_DATASET_REQUIRED_PERMISSIONS)

    camera_settings = CameraSettings(
        camera_name=data["camera_name"],
        min_zoom_factor=data["min_zoom_factor"],
        max_zoom_factor=data["max_zoom_factor"],
        min_quality=data["min_quality"],
        max_quality=data["max_quality"],
        accepted_delay=data["accepted_delay"],
        target_fps_factor=data["target_fps_factor"]
    )

    deltascope = DeltascopeResource(name=data["name"], description=description, owner=user,
                                    project=project_collaborator.project,
                                    marlin_manager_name=data["marlin_manager_name"],
                                    com_port=data["com_port"],
                                    baud_rate=data["baud_rate"], max_radius=data["max_radius"], min_z=data["min_z"],
                                    max_z=data["max_z"], max_acc=data["max_acc"], speed=data["speed"],
                                    global_dataset=ObjectId(data["global_dataset_id"]),
                                    local_dataset=ObjectId(data["local_dataset_id"]),
                                    camera_settings=camera_settings)

    if deltascope.resource_name_exists(data["name"]):
        return exception_json(Exception("Resource name already taken."), error_id=ErrorID.RESOURCE_NAME_ALREADY_PRESENT)

    try:
        deltascope.save()
        resource_collaborator = ResourceCollaborator(resource=deltascope, user=user,
                                                     project=project_collaborator.project,
                                                     permissions={
                                                         "resource": {PERMISSION_WILDCARD: True}
                                                     })
        resource_collaborator.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Resource added."}), 201


@deltascope_bp.route("/set", methods=["PATCH"])
@validate_request(schema={
    Optional("name"): str,
    Optional("marlin_manager_name"): str,
    Optional("com_port"): str,
    Optional("camera_name"): str,
    Optional("baud_rate"): int,
    Optional("description"): str,
    Optional("max_radius"): Or(int, float),
    Optional("min_z"): Or(int, float),
    Optional("max_z"): Or(int, float),
    Optional("max_acc"): Or(int, float),
    Optional("speed"): Or(int, float),
    Optional("min_zoom_factor"): Or(int, float),
    Optional("max_zoom_factor"): Or(int, float),
    Optional("min_quality"): Or(int, float),
    Optional("max_quality"): Or(int, float),
    Optional("accepted_delay"): Or(int, float),
    Optional("target_fps_factor"): Or(int, float),
    Optional("global_dataset_id"): str,
    Optional("local_dataset_id"): str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_set(data: dict, user: User, project_collaborator: ProjectCollaborator,
                   resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource

    if "name" in data:
        deltascope.name = data["name"]

    if "marlin_manager_name" in data:
        deltascope.marlin_manager_name = data["marlin_manager_name"]
        if MARLIN_CACHE.has(project_collaborator.project.id, resource_collaborator.resource.id):
            MARLIN_CACHE.remove(project_collaborator.project.id, resource_collaborator.resource.id)

    if "com_port" in data:
        deltascope.com_port = data["com_port"]
        if MARLIN_CACHE.has(project_collaborator.project.id, resource_collaborator.resource.id):
            MARLIN_CACHE.remove(project_collaborator.project.id, resource_collaborator.resource.id)

    if "camera_name" in data:
        if data["camera_name"] != deltascope.camera_settings.camera_name:
            deltascope.camera_settings.camera_name = data["camera_name"]
            CAMERA_CACHE.remove(project_collaborator.project.id, resource_collaborator.resource.id)

    if "baud_rate" in data:
        deltascope.baud_rate = data["baud_rate"]
        if MARLIN_CACHE.has(project_collaborator.project.id, resource_collaborator.resource.id):
            MARLIN_CACHE.remove(project_collaborator.project.id, resource_collaborator.resource.id)

    if "description" in data:
        deltascope.description = data["description"]

    if "max_radius" in data:
        deltascope.max_radius = data["max_radius"]

    if "max_z" in data:
        deltascope.max_z = data["max_z"]

    if "min_z" in data:
        deltascope.min_z = data["min_z"]

    if "max_acc" in data:
        deltascope.max_acc = data["max_acc"]

    if "speed" in data:
        deltascope.speed = data["speed"]

    if "min_zoom_factor" in data:
        deltascope.camera_settings.min_zoom_factor = data["min_zoom_factor"]

    if "max_zoom_factor" in data:
        deltascope.camera_settings.max_zoom_factor = data["max_zoom_factor"]

    if "min_quality" in data:
        deltascope.camera_settings.min_quality = data["min_quality"]

    if "max_quality" in data:
        deltascope.camera_settings.max_quality = data["max_quality"]

    if "accepted_delay" in data:
        deltascope.camera_settings.accepted_delay = data["accepted_delay"]

    if "target_fps_factor" in data:
        deltascope.camera_settings.target_fps_factor = data["target_fps_factor"]

    if "global_dataset_id" in data and ObjectId(data["global_dataset_id"]) != deltascope.global_dataset.id:
        check_permissions_on_dataset(user, data["global_dataset_id"], GLOBAL_DATASET_REQUIRED_PERMISSIONS)
        deltascope.global_dataset = ObjectId(data["global_dataset_id"])

    if "local_dataset_id" in data and ObjectId(data["local_dataset_id"]) != deltascope.local_dataset.id:
        check_permissions_on_dataset(user, data["local_dataset_id"], LOCAL_DATASET_REQUIRED_PERMISSIONS)
        deltascope.local_dataset = ObjectId(data["local_dataset_id"])

    if MARLIN_CACHE.has(project_collaborator.project.id, resource_collaborator.resource.id):
        manager = MARLIN_CACHE.get(project_collaborator.project.id, resource_collaborator.resource.id)
        manager.set_max_acceleration(deltascope.max_acc)
        manager.set_work_space_check(DeltaWorkSpaceCheck(deltascope.max_radius, deltascope.min_z, deltascope.max_z))
        manager.set_speed(deltascope.speed)

    if deltascope.resource_name_exists(data["name"], resource_id=deltascope.id):
        return exception_json(Exception("Resource name already taken."), error_id=ErrorID.RESOURCE_NAME_ALREADY_PRESENT)

    try:
        deltascope.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Resource set."}), 200


@deltascope_bp.route("/movement_detection/set", methods=["PATCH"])
@validate_request(schema={
    Optional("resize_factor"): Or(float, int),
    Optional("threshold"): Or(float, int),
    Optional("time_dist"): int,
    Optional("closing_kernel_size"): int,
    Optional("opening_kernel_size"): int,
    Optional("contour_length_threshold"): int,
    Optional("denoise_filter_size"): int,
    Optional("attentive_factor"): Or(float, int),
    Optional("attention_mode"): str,
    Optional("attention_persistence"): Or(float, int)
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_movement_detection_set(data: dict, user: User, project_collaborator: ProjectCollaborator,
                                      resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource

    if "resize_factor" in data:
        deltascope.camera_settings.diff_resize_factor = float(data["resize_factor"])

    if "threshold" in data:
        deltascope.camera_settings.diff_threshold = float(data["threshold"])

    if "time_dist" in data:
        deltascope.camera_settings.diff_time_dist = data["time_dist"]

    if "closing_kernel_size" in data:
        deltascope.camera_settings.diff_closing_kernel_size = data["closing_kernel_size"]

    if "opening_kernel_size" in data:
        deltascope.camera_settings.diff_opening_kernel_size = data["opening_kernel_size"]

    if "contour_length_threshold" in data:
        deltascope.camera_settings.diff_contour_length_threshold = data["contour_length_threshold"]

    if "denoise_filter_size" in data:
        deltascope.camera_settings.diff_denoise_filter_size = data["denoise_filter_size"]

    if "attentive_factor" in data:
        deltascope.camera_settings.diff_attentive_factor = float(data["attentive_factor"])

    if "attention_mode" in data:
        deltascope.camera_settings.diff_attention_mode = data["attention_mode"]

    if "attention_persistence" in data:
        deltascope.camera_settings.diff_attention_persistence = float(data["attention_persistence"])

    try:
        deltascope.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Resource set."}), 200


@deltascope_bp.route("/default/get", methods=["GET", "POST"])
@validate_request(schema={})
def deltascope_default_get(data: dict):
    return jsonify({"defaults": DELTASCOPE_DEFAULT_SETTINGS}), 200


@deltascope_bp.route("/position/add", methods=["POST"])
@validate_request(schema={
    "name": str,
    "x": Or(int, float),
    "y": Or(int, float),
    "z": Or(int, float)
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_position_add(data: dict, user: User, project_collaborator: ProjectCollaborator,
                            resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource

    position_data = {
        "name": data["name"],
        "x": data["x"],
        "y": data["y"],
        "z": data["z"],
        "experiment": deltascope.experiment
    }

    if CAMERA_CACHE.has(project_collaborator.project.id, resource_collaborator.resource.id):
        camera_data: CameraManager = CAMERA_CACHE.get(project_collaborator.project.id,
                                                      resource_collaborator.resource.id)
        position_data["zoom"] = camera_data.zoom_factor
        rdm = ResourceDataManager.get()
        pid = project_collaborator.project.id
        rid = resource_collaborator.resource.id
        path = RESOURCE_TEMP_PATH
        name = str(uuid4()) + ".mp4"
        url = rdm.get_url(pid, rid, path, name)
        rdm.add_data(pid, rid, path, "empty", bytes())
        frames_generator_to_video(camera_data.get_old_frames_generator(), camera_data.get_fps(), url)
        video_bytes = rdm.get_data(pid, rid, path, name)
        video_b64 = base64_encode(video_bytes, as_string=True, data_format="video/mp4")

        dataset_element_data = {
            "base64": video_b64,
            "metadata": position_data
        }

        global_dataset = DatasetResource.objects(id=deltascope.global_dataset.id).first()
        direct_dataset_element_add(dataset_element_data, global_dataset)

        local_dataset = DatasetResource.objects(id=deltascope.local_dataset.id).first()
        direct_dataset_element_add(dataset_element_data, local_dataset)

    return jsonify({"msg": "Position added."}), 201


@deltascope_bp.route("/position/del", methods=["DELETE"])
@validate_request(schema={
    "position_id": str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_position_del(data: dict, user: User, project_collaborator: ProjectCollaborator,
                            resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource
    element: DatasetElement = DatasetElement.objects(id=data["position_id"],
                                                     dataset=deltascope.local_dataset.id).first()

    direct_dataset_element_del(element)

    return jsonify({"msg": "Position deleted."}), 200


@deltascope_bp.route("/position/clear", methods=["DELETE"])
@validate_request(schema={
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_position_clear(data: dict, user: User, project_collaborator: ProjectCollaborator,
                              resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource
    dataset: DatasetResource = deltascope.local_dataset

    direct_dataset_clear(dataset)

    return jsonify({"msg": "Positions cleared."}), 201


@deltascope_bp.route("/position/get", methods=["GET", "POST"])
@validate_request({
    Optional("incorporate_preview"): bool
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.get"])
@mongo_query_validate()
def deltascope_position_get(data: dict, user: User, project_collaborator: ProjectCollaborator,
                            resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource
    dataset: DatasetResource = deltascope.local_dataset

    res, count = direct_dataset_element_get(dataset, data)

    return list_to_flask_response(res, count)


@deltascope_bp.route("/experiment/set", methods=["PATCH"])
@validate_request(schema={
    "experiment": str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_experiment_set(data: dict, user: User, project_collaborator: ProjectCollaborator,
                              resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource

    deltascope.experiment = data["experiment"]

    deltascope.save()

    return jsonify({"msg": "Experiment set"}), 200


@deltascope_bp.route("/search_area/set", methods=["PATCH"])
@validate_request(schema={
    "p1": list,
    "p2": list,
    "p3": list,
    "center": list,
    "radius": Or(int, float)
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.set"])
def deltascope_search_area_set(data: dict, user: User, project_collaborator: ProjectCollaborator,
                               resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource

    deltascope.search_area = SearchArea(
        p1=data["p1"],
        p2=data["p2"],
        p3=data["p3"],
        center=data["center"],
        radius=float(data["radius"])
    )

    deltascope.save()

    return jsonify({"msg": "Search area set"}), 200


@deltascope_bp.route("/search_area/get", methods=["GET", "POST"])
@validate_request(schema={})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.get"])
def deltascope_search_area_get(data: dict, user: User, project_collaborator: ProjectCollaborator,
                               resource_collaborator: ResourceCollaborator):
    deltascope: DeltascopeResource = resource_collaborator.resource

    search_area: SearchArea = deltascope.search_area

    search_area = mongo_document_to_json(search_area)

    pprint(search_area)

    return jsonify(search_area), 200
