import os
import shutil
from abc import ABC, abstractmethod
from typing import List
from uuid import uuid4

from schema import Optional

from common import base64_encode
from common.base64_encode import base64_decode
from common.decorators.decorators import validate_request
from common.decorators.permissions import has_permissions_on_resource
from common.decorators.requirements import requires_login, requires_project, requires_resource
from db.projects import ProjectCollaborator
from db.resources.base import ResourceCollaborator
from db.users import User
from general_utils.files_and_folders.paths import is_subpath
from flask import Blueprint, jsonify

resdata_bp = Blueprint("res_data", __name__, url_prefix="/api/resource/data")


class ResourceDataEntry:

    def __init__(self, name, isresource):
        self._name = name
        self._isresource = isresource

    def get_name(self):
        return self._name

    def is_resource(self):
        return self._isresource

    def to_json(self):
        return {
            "name": self.get_name(),
            "type": "resource" if self.is_resource() else "path"
        }


class ResourceDataManager(ABC):
    _instance = None

    @abstractmethod
    def add_data(self, project_id: str, resource_id: str, relative_path: str, name: str, data: bytes):
        pass

    @abstractmethod
    def get_data(self, project_id: str, resource_id: str, relative_path: str, name: str):
        pass

    @abstractmethod
    def exists_data(self, project_id: str, resource_id: str, relative_path: str, name: str):
        pass

    @abstractmethod
    def remove_data(self, project_id: str, resource_id: str, relative_path: str, name: str):
        pass

    @abstractmethod
    def remove_path(self, project_id: str, resource_id: str, relative_path: str):
        pass

    @abstractmethod
    def get_url(self, project_id: str, resource_id: str, relative_path: str, name: str):
        pass

    @abstractmethod
    def get_data_list(self, project_id: str, resource_id: str, relative_path: str) -> List[
        ResourceDataEntry]:
        pass

    @classmethod
    def set(cls, resource_data_manager: "ResourceDataManager"):
        if cls._instance is None:
            cls._instance = resource_data_manager

    @classmethod
    def get(cls) -> "ResourceDataManager":
        return cls._instance


class FilesystemResourceDataManager(ResourceDataManager):

    def __init__(self, base_folder):
        self._base_folder = base_folder

    def get_full_path(self, project_id, resource_id, relative_path, name=""):
        full_base_path = os.path.join(self._base_folder, "project_%s" % project_id, "resource_%s" % resource_id)
        full_path = os.path.join(full_base_path, relative_path, name)
        if not is_subpath(full_path, full_base_path):
            raise PermissionError("Can't go outside of the base path.")

        return full_path

    def add_data(self, project_id, resource_id, relative_path, name, data):
        full_path = self.get_full_path(project_id, resource_id, relative_path, name)
        print("FULL_PATH", full_path)
        if not os.path.exists(os.path.dirname(full_path)):
            os.makedirs(os.path.dirname(full_path))

        with open(full_path, "wb") as f:
            f.write(data)

    def get_data(self, project_id, resource_id, relative_path, name):
        full_path = self.get_full_path(project_id, resource_id, relative_path, name)

        with open(full_path, "rb") as f:
            return f.read()

    def remove_data(self, project_id: str, resource_id: str, relative_path: str, name):
        full_path = self.get_full_path(project_id, resource_id, relative_path, name)

        if os.path.isfile(full_path):
            os.remove(full_path)

    def remove_path(self, project_id: str, resource_id: str, relative_path: str):
        full_path = self.get_full_path(project_id, resource_id, relative_path)

        if os.path.isdir(full_path):
            shutil.rmtree(full_path)

    def exists_data(self, project_id: str, resource_id: str, relative_path: str, name):
        full_path = self.get_full_path(project_id, resource_id, relative_path, name)

        return os.path.isfile(full_path)

    def get_data_list(self, project_id, resource_id, relative_path):
        full_path = self.get_full_path(project_id, resource_id, relative_path)

        res = []

        for filename in os.listdir(full_path):
            isfile = False
            if os.path.isfile(os.path.join(full_path, filename)):
                isfile = True
            res.append(ResourceDataEntry(filename, isfile))

        return res

    def get_url(self, project_id: str, resource_id: str, relative_path: str, name: str):
        return os.path.join(self._base_folder, self.get_full_path(project_id, resource_id, relative_path, name))


def _res_data_add(data: dict, project_id, resource_id):

    data_to_be_written = b""

    if "text" in data:
        data_to_be_written = data["text"].encode('utf-8')
    elif "base64" in data:
        data_to_be_written = base64_decode(data["base64"])

    name = data.get("name", str(uuid4()))

    path = data.get("path", "")

    ResourceDataManager.get().add_data(project_id, resource_id, path, name, data_to_be_written)

    return {"path": path, "name": name}


@resdata_bp.route("/add", methods=["POST"])
@validate_request({
    Optional("text"): str,
    Optional("base64"): str,
    Optional("path"): str,
    Optional("name"): str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.data.add"])
def res_data_add(data: dict, user: User, project_collaborator: ProjectCollaborator,
                 resource_collaborator: ResourceCollaborator):
    res = _res_data_add(data, project_collaborator.project.id, resource_collaborator.resource.id)
    res["msg"] = "Resource data added."
    return jsonify(res), 200


def _res_data_del(data: dict, project_id, resource_id):

    name = data.get("name", None)

    path = data.get("path", None)

    if name is None and path is None:
        path = ""

    if name is None:
        ResourceDataManager.get().remove_path(project_id, resource_id, path)
    else:
        if path is None:
            path = ""
        ResourceDataManager.get().remove_data(project_id, resource_id, path, name)


@resdata_bp.route("/del", methods=["delete"])
@validate_request({
    Optional("path"): str,
    Optional("name"): str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.data.del"])
def res_data_del(data: dict, user: User, project_collaborator: ProjectCollaborator,
                 resource_collaborator: ResourceCollaborator):
    _res_data_del(data, project_collaborator.project.id, resource_collaborator.resource.id)
    return jsonify({"msg": "Resource data deleted."}), 200


def _res_data_get(data: dict, project_id, resource_id):

    path = data.get("path", "")
    name = data["name"]
    read_as_text = data.get("read_as_text", False)

    data = ResourceDataManager.get().get_data(project_id, resource_id, path, name)

    if read_as_text:
        data = data.decode('utf-8')
    else:
        data = base64_encode.base64_encode(data).decode('ascii')

    return data


@resdata_bp.route("/get", methods=["GET", "POST"])
@validate_request({
    "name": str,
    Optional("path"): str,
    Optional("read_as_text"): bool
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.data.get"])
def res_data_get(data: dict, user: User, project_collaborator: ProjectCollaborator,
                 resource_collaborator: ResourceCollaborator):
    data = _res_data_get(data, project_collaborator.project.id, resource_collaborator.resource.id)
    return jsonify({"data": data}), 200


def _res_data_list(data: dict, project_id, resource_id):

    path = data.get("path", "")

    reslist = [r.to_json() for r in ResourceDataManager.get().get_data_list(project_id, resource_id, path)]

    return reslist


@resdata_bp.route("/list", methods=["GET", "POST"])
@validate_request({
    Optional("path"): str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.data.list"])
def res_data_list(data: dict, user: User, project_collaborator: ProjectCollaborator,
                  resource_collaborator: ResourceCollaborator):
    reslist = _res_data_list(data, project_collaborator.project.id, resource_collaborator.resource.id)

    return jsonify({"resource_list": reslist}), 200
