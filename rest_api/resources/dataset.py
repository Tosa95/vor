from datetime import datetime
from pprint import pprint

from flask import Blueprint, jsonify
from schema import Optional

from common.codes import PERMISSION_WILDCARD
from common.decorators.decorators import validate_request, mongo_query_validate
from common.decorators.permissions import has_permissions_on_project, has_permissions_on_resource
from common.decorators.requirements import requires_login, requires_project, requires_resource
from common.errors import exception_json, ErrorID
from common.mongo import mongo_query, mongo_query_result_to_json, raw_mongo_query, list_to_flask_response
from db.projects import ProjectCollaborator
from db.resources.base import ResourceCollaborator
from db.resources.dataset import DatasetElementTypes, DatasetResource, DatasetElement, VideoDatasetResource, \
    TextDatasetResource, ImageDatasetResource
from db.users import User
from rest_api.resources.data import _res_data_add, _res_data_get, _res_data_del, ResourceDataManager
from rest_api.resources.dataset_element_transformers import TextDatasetElementTransformer, \
    ImageDatasetElementTransformer, \
    DatasetElementTransformer, VideoDatasetElementTransformer
from settings.multimedia import MultimediaSettings

dataset_bp = Blueprint("dataset", __name__, url_prefix="/api/resource/dataset")

ELEMENTS_RESOURCE_DATA_PATH = "elements"

LOAD_METHOD_KEY = "load_method"
PREVIEWER_KEY = "previewer"
DOCUMENT_TYPE_KEY = "document_type"


class DatasetElementTypeData:

    def __init__(self, load_method: str, previewer: DatasetElementTransformer,
                 standard_transformer: DatasetElementTransformer, document: type(DatasetResource)):
        self.load_method = load_method
        self.previewer = previewer
        self.standard_transformer = standard_transformer
        self.document = document


# TODO: add user to dataset element

class DatasetElementTypeLoadMethod:
    TEXT = "text"
    BASE64 = "base64"


def get_element_type_data():
    return {
        DatasetElementTypes.TEXT: DatasetElementTypeData(
            load_method=DatasetElementTypeLoadMethod.TEXT,
            previewer=TextDatasetElementTransformer(),
            standard_transformer=TextDatasetElementTransformer(),
            document=TextDatasetResource
        ),
        DatasetElementTypes.IMAGE: DatasetElementTypeData(
            load_method=DatasetElementTypeLoadMethod.BASE64,
            previewer=ImageDatasetElementTransformer(),
            standard_transformer=ImageDatasetElementTransformer(),
            document=ImageDatasetResource
        ),
        DatasetElementTypes.VIDEO: DatasetElementTypeData(
            load_method=DatasetElementTypeLoadMethod.BASE64,
            previewer=VideoDatasetElementTransformer(
                output_format=MultimediaSettings.get().video_format,
                video_codec=MultimediaSettings.get().video_codec
            ),
            standard_transformer=VideoDatasetElementTransformer(
                output_format=MultimediaSettings.get().video_format,
                video_codec=MultimediaSettings.get().video_codec
            ),
            document=VideoDatasetResource
        ),
        DatasetElementTypes.AUDIO: DatasetElementTypeData(
            load_method=DatasetElementTypeLoadMethod.BASE64,
            # TODO: audio previewer
            previewer=None,
            standard_transformer=None,
            document=DatasetResource
        ),
    }


def get_previewer(dataset: DatasetResource):
    transform_data = dataset.preview_transform_params
    previewer = get_element_type_data()[dataset.element_type].previewer.load_from_document(transform_data)
    return previewer


def get_standard_transformer(dataset: DatasetResource):
    transform_data = dataset.standard_transform_params
    transformer = get_element_type_data()[dataset.element_type].standard_transformer.load_from_document(
        transform_data)
    return transformer


def compute_preview(dataset: DatasetResource, element_path, element_name):
    previewer: DatasetElementTransformer = get_previewer(dataset)

    preview_name = element_name + previewer.get_preview_suffix()

    rm = ResourceDataManager.get()

    project_id = dataset.project.id
    resource_id = dataset.id

    if not rm.exists_data(project_id, resource_id, element_path, preview_name):
        element_data = rm.get_data(project_id, resource_id, element_path, element_name)

        preview_data = previewer.transform(element_data)

        rm.add_data(project_id, resource_id, element_path, preview_name, preview_data)

    return preview_name


def compute_standard_transform(dataset: DatasetResource, element_path, element_name):
    transformer: DatasetElementTransformer = get_standard_transformer(dataset)

    rm = ResourceDataManager.get()

    project_id = dataset.project.id
    resource_id = dataset.id

    element_data = rm.get_data(project_id, resource_id, element_path, element_name)

    transformed_data = transformer.transform(element_data)

    rm.add_data(project_id, resource_id, element_path, element_name, transformed_data)

    return element_name


@dataset_bp.route("/add", methods=["POST"])
@validate_request(schema={
    "name": str,
    Optional("description"): str,
    "element_type": str
})
@requires_login()
@requires_project()
@has_permissions_on_project(["resource.dataset.add"])
def dataset_add(data: dict, user: User, project_collaborator: ProjectCollaborator):
    if data["element_type"] not in get_element_type_data():
        return exception_json(Exception("The element type %s is not supported." % data["element_type"]),
                              error_id=ErrorID.DATASET_ELEMENT_TYPE_NOT_SUPPORTED)

    description = data.get("description", None)

    dataset = get_element_type_data()[data["element_type"]].document(name=data["name"], description=description,
                                                                     element_type=data["element_type"],
                                                                     project=project_collaborator.project.id)

    if dataset.resource_name_exists(data["name"]):
        return exception_json(Exception("Resource name already taken."), error_id=ErrorID.RESOURCE_NAME_ALREADY_PRESENT)

    try:
        dataset.save()
        resource_collaborator = ResourceCollaborator(resource=dataset, user=user,
                                                     project=project_collaborator.project,
                                                     permissions={
                                                         "resource": {PERMISSION_WILDCARD: True}
                                                     })
        resource_collaborator.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Resource added."}), 201


@dataset_bp.route("/set", methods=["PATCH"])
@validate_request(schema={
    Optional("name"): str,
    Optional("description"): str,
    Optional("element_type"): str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_project(["resource.set"])
def dataset_set(data: dict, user: User, project_collaborator: ProjectCollaborator,
                resource_collaborator: ResourceCollaborator):
    dataset: DatasetResource = resource_collaborator.resource

    if "name" in data:
        if dataset.resource_name_exists(data["name"], resource_id=dataset.id):
            return exception_json(Exception("Resource name already taken."),
                                  error_id=ErrorID.RESOURCE_NAME_ALREADY_PRESENT)
        dataset.name = data["name"]

    if "description" in data:
        dataset.description = data["description"]

    if "element_type" in data:
        dataset.element_type = data["element_type"]

    dataset.save()

    return jsonify({"msg": "Resource set."}), 200


def direct_dataset_element_add(data: dict, dataset: DatasetResource):
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!
    # TODO: bucketize items to avoid having too much elements in the same directory!

    if "text" not in data and "base64" not in data:
        return exception_json(Exception("text and base64 can't both be none: you must supply data!"),
                              error_id=ErrorID.REQUEST_VALIDATION_FAILED)

    created = datetime.fromtimestamp(data.get("created_timestamp", datetime.now().timestamp()))

    dataset_element = DatasetElement(dataset=dataset, created=created)

    dataset_element.save()

    try:
        resource_data_name = str(dataset_element.id)
        resource_data_path = ELEMENTS_RESOURCE_DATA_PATH

        dataset_element.data_name = resource_data_name
        dataset_element.data_path = resource_data_path

        add_resource_data_data = {
            "name": resource_data_name,
            "path": resource_data_path
        }

        if "text" in data:
            add_resource_data_data["text"] = data["text"]

        if "base64" in data:
            base64_split = data["base64"].split(",")
            if len(base64_split) > 1:
                add_resource_data_data["base64"] = base64_split[1]
                dataset_element.data_format = base64_split[0]
            else:
                add_resource_data_data["base64"] = base64_split[0]

        if "metadata" in data:
            dataset_element.metadata = data["metadata"]

        _res_data_add(add_resource_data_data, dataset.project.id, dataset.id)

        compute_preview(dataset, resource_data_path, resource_data_name)
        compute_standard_transform(dataset, resource_data_path, resource_data_name)

        dataset_element.save()
    except Exception as e:
        dataset_element.delete()
        raise e

    return resource_data_path, resource_data_name, dataset_element


@dataset_bp.route("/element/add", methods=["POST"])
@validate_request(schema={
    Optional("text"): str,
    Optional("base64"): str,
    Optional("created_timestamp"): int,
    Optional("metadata"): dict
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.element.add"])
def dataset_element_add(data: dict, user: User, project_collaborator: ProjectCollaborator,
                        resource_collaborator: ResourceCollaborator):
    dataset: DatasetResource = resource_collaborator.resource

    resource_data_path, resource_data_name, dataset_element = direct_dataset_element_add(data, dataset)

    return jsonify({"id": str(dataset_element.id), "path": resource_data_path, "name": resource_data_name}), 200


def direct_dataset_element_get(dataset: DatasetResource, data: dict):
    incorporate_data = data.get("incorporate_data", False)
    incorporate_preview = data.get("incorporate_preview", False)

    project_id = dataset.project.id
    resource_id = dataset.id

    print(incorporate_data, incorporate_preview)

    if "query" not in data:
        data["query"] = {}

    data["query"]["dataset"] = str(dataset.id)

    if not incorporate_data and not incorporate_preview:
        res, count = raw_mongo_query(DatasetElement, data)
        return mongo_query_result_to_json(res), count
    else:
        res, count = raw_mongo_query(DatasetElement, data)
        res = mongo_query_result_to_json(res)

        for element in res:
            try:
                name = element["data_name"]
                path = element["data_path"]

                if incorporate_data:
                    element_data = _res_data_get(
                        {
                            "name": name,
                            "path": path,
                            "read_as_text": True if get_element_type_data()[
                                                        dataset.element_type].load_method == DatasetElementTypeLoadMethod.TEXT else False
                        },
                        project_id,
                        resource_id
                    )

                    element["data"] = element_data
                elif incorporate_preview:
                    preview_name = compute_preview(dataset, path, name)

                    preview_data = _res_data_get(
                        {
                            "name": preview_name,
                            "path": path,
                            "read_as_text": True if get_element_type_data()[
                                                        dataset.element_type].load_method == DatasetElementTypeLoadMethod.TEXT else False
                        },
                        project_id,
                        resource_id
                    )

                    element["preview_data"] = preview_data
            except Exception as e:
                if "metadata" not in element:
                    element["metadata"] = {}
                element["metadata"]["data_error"] = "Couldn't incorporate element data: %s" % str(e)

        return res, count


@dataset_bp.route("/element/get", methods=["GET", "POST"])
@validate_request({
    Optional("incorporate_data"): bool,
    Optional("incorporate_preview"): bool
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.element.get"])
@mongo_query_validate()
def dataset_element_get(data: dict, user: User, project_collaborator: ProjectCollaborator,
                        resource_collaborator: ResourceCollaborator):
    dataset: DatasetResource = resource_collaborator.resource

    res, count = direct_dataset_element_get(dataset, data)

    return list_to_flask_response(res, count)


def direct_dataset_element_del(element: DatasetElement):
    project_id = element.dataset.project.id
    resource_id = element.dataset.id

    name = element.data_name
    path = element.data_path

    if name is not None and path is not None and name != "" and path != "":
        # Deleting data only if it is missing

        _res_data_del(
            {
                "name": name,
                "path": path,
            },
            project_id,
            resource_id
        )

    element.delete()


@dataset_bp.route("/element/del", methods=["DELETE"])
@validate_request({
    "element_id": str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.element.del"])
def dataset_element_del(data: dict, user: User, project_collaborator: ProjectCollaborator,
                        resource_collaborator: ResourceCollaborator):
    element: DatasetElement = DatasetElement.objects(id=data["element_id"],
                                                     dataset=resource_collaborator.resource.id).first()

    direct_dataset_element_del(element)

    return jsonify({"msg": "Element deleted."}), 200


def direct_dataset_clear(dataset: DatasetResource):
    DatasetElement.objects(dataset=dataset.id).delete()


@dataset_bp.route("/clear", methods=["DELETE"])
@validate_request()
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.clear"])
def dataset_clear(data: dict, user: User, project_collaborator: ProjectCollaborator,
                  resource_collaborator: ResourceCollaborator):
    dataset = resource_collaborator.resource

    direct_dataset_clear(dataset)

    return jsonify({"msg": "Dataset cleared."}), 200


@dataset_bp.route("/element_type/get", methods=["GET"])
@validate_request(schema={})
def dataset_element_type_get(data: dict):
    return jsonify({"objects": list(get_element_type_data().keys())}), 200
