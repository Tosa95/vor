from datetime import datetime
from threading import RLock

from schema import Optional, Or

from camera.cameras.camera_cache import CAMERA_CACHE
from common.camera.camera_manager import CameraManager
from common.marlin.commands import LinearAbsoluteMoveMarlinCommand, LinearRelativeMoveMarlinCommand, \
    AutoHomeMarlinCommand
from common.marlin.remote_marlin_manager import RemoteMarlinManager
from common.mongo import mongo_document_to_json
from db.projects import ProjectCollaborator
from db.resources.base import ResourceCollaborator
from db.resources.deltascope import DeltascopeResource
from db.users import User
from rest_api.websocket_utils import BaseNamespace
from common.codes import generate_resource_entity_key
from common.decorators.decorators import validate_request, excorporate_self, incorporate_self, cached_resource_authorize
from common.decorators.requirements import requires_login, requires_project, requires_resource
from common.decorators.permissions import has_permissions_on_resource
from common.decorators.compound_decorator import compound_decorator
from marlin_connector.marlin_cache import MARLIN_CACHE
from settings.orchestrator import Orchestrator

view_room_validator = [
    validate_request(ws=True),
    requires_login(ws=True),
    requires_project(ws=True),
    requires_resource(ws=True),
    has_permissions_on_resource(["resource.control.view"], ws=True)
]

move_validator = [
    validate_request(ws=True),
    requires_login(ws=True),
    requires_project(ws=True),
    requires_resource(ws=True),
    has_permissions_on_resource(["resource.control.move"], ws=True)
]


class DeltascopeNamespace(BaseNamespace):

    def __init__(self, namespace):
        super().__init__(namespace)

        self._cam_lock = RLock()
        self._marlin_lock = RLock()

    def get_status(self, project_id, resource_id):

        enable_movement_detection = True

        if MARLIN_CACHE.has(project_id, resource_id):
            manager: RemoteMarlinManager = MARLIN_CACHE.get(project_id, resource_id)
            status = manager.get_status()
            if status["commands_to_be_completed"] > 0:
                enable_movement_detection = False
                print("HEREEEEE")
        else:
            status = RemoteMarlinManager.get_empty_status()
            status["error"] = True
            status["error_cause"] = "Robot disconnected. Reconnect it and reload the page."

        if CAMERA_CACHE.has(project_id, resource_id):
            cam_data: CameraManager = CAMERA_CACHE.get(project_id, resource_id)

            if enable_movement_detection:

                if cam_data.time_from_last_movement is None:
                    cam_data.time_from_last_movement = datetime.now()
                    cam_data.enable_movement_detection = False
                elif cam_data.time_from_last_movement is not None and (
                        datetime.now() - cam_data.time_from_last_movement).total_seconds() > 0.5:
                    cam_data.enable_movement_detection = True
                else:
                    cam_data.enable_movement_detection = False

            else:

                cam_data.time_from_last_movement = None
                cam_data.enable_movement_detection = False

            status["cam_data"] = cam_data.get_json_status()
        # pprint(status)
        return status

    def emit_status(self, project_id, resource_id):
        self.emit("status", self.get_status(project_id, resource_id),
                  room=generate_resource_entity_key(project_id, resource_id, "status"), include_self=True)

    def get_marlin_remote_manager(self, project_id, resource_id) -> RemoteMarlinManager:
        print("INITIALIZING MARLIN")

        with self._marlin_lock:
            if not MARLIN_CACHE.has(project_id, resource_id):
                deltascope = DeltascopeResource.objects(id=resource_id).first()

                marlin_manager = RemoteMarlinManager(
                    Orchestrator.get(),
                    mongo_document_to_json(deltascope),
                    blocking=True
                )

                def on_marlin_status_changed(manager):
                    self.emit_status(project_id, resource_id)
                    if manager.get_status()["error"]:
                        MARLIN_CACHE.remove(project_id, resource_id)

                marlin_manager.get_status_changed_event().add_listener(on_marlin_status_changed)

                marlin_manager.start()

                print("REMOTE CREATED")

                MARLIN_CACHE.add(project_id, resource_id, marlin_manager)

            print("DONE INITIALIZING MARLIN")

            return MARLIN_CACHE.get(project_id, resource_id)

    def get_camera_data(self, pipeline_name, deltascope: DeltascopeResource):
        return CameraManager(Orchestrator.get(), deltascope.camera_settings.get_camera_data(), pipeline_name)

    def initialize_camera(self, deltascope: DeltascopeResource, project_id, resource_id):

        print("INITIALIZING CAMERA")

        with self._cam_lock:
            if not CAMERA_CACHE.has(project_id, resource_id):

                def on_frame(frame, metadata):
                    frame = frame.decode('ascii')

                    data_to_send = {
                        "frame": "data:image/jpg;base64," + frame,
                        "metadata": metadata
                    }

                    self.emit("frame", data_to_send,
                              room=generate_resource_entity_key(project_id, resource_id, "view"))

                def on_camera_needs_update(camera_manager: CameraManager):
                    deltascope = DeltascopeResource.objects(project=project_id, id=resource_id).first()
                    camera_manager.load_from_camera_data(deltascope.camera_settings.get_camera_data())

                def on_camera_metrics_changed(camera_manager: CameraManager):
                    self.emit_status(project_id, resource_id)

                def on_no_frame_received(camera_manager: CameraManager):
                    camera_manager.unsubscribe()
                    CAMERA_CACHE.remove(project_id,resource_id)

                pipeline_name = generate_resource_entity_key(project_id, resource_id, "view")

                camera_manager = self.get_camera_data(pipeline_name, deltascope)

                camera_manager.frame_event.add_listener(on_frame)
                camera_manager.metrics_changed_event.add_listener(on_camera_metrics_changed)
                camera_manager.needs_update_event.add_listener(on_camera_needs_update)
                camera_manager.no_frames_received_event.add_listener(on_no_frame_received)

                camera_manager.start()

                CAMERA_CACHE.add(project_id, resource_id, camera_manager)

        print("DONE INITIALIZING CAMERA")

    @excorporate_self()
    @compound_decorator(view_room_validator)
    @incorporate_self()
    def on_join_view(self, data, user: User, project_collaborator: ProjectCollaborator,
                     resource_collaborator: ResourceCollaborator):
        room_name = generate_resource_entity_key(project_collaborator.project.id, resource_collaborator.resource.id,
                                                 "view")
        self.initialize_camera(resource_collaborator.resource, project_collaborator.project.id,
                               resource_collaborator.resource.id)
        return self.manage_room_join(room_name, data, view_room_validator)

    @excorporate_self()
    @compound_decorator(view_room_validator)
    @incorporate_self()
    def on_join_status(self, data, user: User, project_collaborator: ProjectCollaborator,
                       resource_collaborator: ResourceCollaborator):
        room_name = generate_resource_entity_key(project_collaborator.project.id, resource_collaborator.resource.id,
                                                 "status")

        res = self.manage_room_join(room_name, data, view_room_validator)

        self.get_marlin_remote_manager(project_collaborator.project.id, resource_collaborator.resource.id)
        self.emit_status(project_collaborator.project.id, resource_collaborator.resource.id)

        return res

    @excorporate_self()
    @compound_decorator(move_validator)
    @incorporate_self()
    def on_authorize_move(self, data, user: User, project_collaborator: ProjectCollaborator,
                          resource_collaborator: ResourceCollaborator):
        command_name = generate_resource_entity_key(project_collaborator.project.id, resource_collaborator.resource.id,
                                                    "move")
        self.get_marlin_remote_manager(project_collaborator.project.id, resource_collaborator.resource.id)
        return self.manage_command_authorization(command_name, data, move_validator)

    @excorporate_self()
    @validate_request(schema={
        Optional("x"): Or(float, int),
        Optional("y"): Or(float, int),
        Optional("z"): Or(float, int)
    }, ws=True)
    @cached_resource_authorize("move", ws=True)
    @incorporate_self()
    def on_move(self, data, project_id, resource_id):
        x = data.get("x", None)
        y = data.get("y", None)
        z = data.get("z", None)

        manager = self.get_marlin_remote_manager(project_id, resource_id)

        manager.send_command(LinearAbsoluteMoveMarlinCommand(x=x, y=y, z=z))

        return {"msg": "Moved to %f %f %f" % (x, y, z)}

    @excorporate_self()
    @validate_request(schema={
        Optional("dx"): Or(float, int),
        Optional("dy"): Or(float, int),
        Optional("dz"): Or(float, int)
    }, ws=True)
    @cached_resource_authorize("move", ws=True)
    @incorporate_self()
    def on_move_relative(self, data, project_id, resource_id):
        dx = data.get("dx", None)
        dy = data.get("dy", None)
        dz = data.get("dz", None)

        manager = self.get_marlin_remote_manager(project_id, resource_id)

        manager.send_command(LinearRelativeMoveMarlinCommand(dx=dx, dy=dy, dz=dz))

        return {"msg": "Moved relative %f %f %f" % (dx, dy, dz)}

    @excorporate_self()
    @validate_request(ws=True)
    @cached_resource_authorize("move", ws=True)
    @incorporate_self()
    def on_auto_home(self, data, project_id, resource_id):

        manager = self.get_marlin_remote_manager(project_id, resource_id)

        manager.send_command(AutoHomeMarlinCommand())

        return {"msg": "Auto homed"}

    @excorporate_self()
    @compound_decorator(move_validator)
    @incorporate_self()
    def on_authorize_set_zoom(self, data, user: User, project_collaborator: ProjectCollaborator,
                              resource_collaborator: ResourceCollaborator):
        command_name = generate_resource_entity_key(project_collaborator.project.id, resource_collaborator.resource.id,
                                                    "set_zoom")
        return self.manage_command_authorization(command_name, data, move_validator)

    @excorporate_self()
    @validate_request(schema={
        "zoom_factor": Or(float, int)
    }, ws=True)
    @cached_resource_authorize("set_zoom", ws=True)
    @incorporate_self()
    def on_set_zoom(self, data, project_id, resource_id):
        zoom_factor = float(data["zoom_factor"])

        camera_manager: CameraManager = CAMERA_CACHE.get(project_id, resource_id)

        camera_manager.set_zoom(zoom_factor)

        return {"msg": "Zoom factor set %f" % zoom_factor}
