from unittest import TestCase

from rest_api.resources.data import FilesystemResourceDataManager


class TestFilesystemResourceDataManager(TestCase):
    def test_basic_usage(self):
        rdm = FilesystemResourceDataManager("test_files")

        pid = "111"
        rid = "222"

        res_path = "test_data"
        res_name = "prova.txt"

        rdm.add_data(pid, rid, res_path, res_name, "prova".encode('utf-8'))
        read = rdm.get_data(pid, rid, res_path, res_name).decode('utf-8')

        self.assertEqual(read, "prova")
        self.assertTrue(rdm.exists_data(pid, rid, res_path, res_name))

        rdm.remove_path(pid, rid, "test_data")

        self.assertFalse(rdm.exists_data(pid, rid, res_path, res_name))
