from flask import Blueprint

from db.users import User, has_permission
from settings.orchestrator import Orchestrator
from settings.resources import ResourceInfo, RESOURCES
from common.decorators.decorators import validate_request, mongo_query_validate
from common.decorators.requirements import requires_login, requires_project, requires_resource
from common.decorators.permissions import has_permissions_on_resource, has_dynamic_permissions, PermissonLevel, \
    has_permissions
from common.errors import exception_json, ErrorID
from common.mongo import list_query, list_to_flask_response, mongo_query_result_to_json

external_resource_bp = Blueprint("external_resource", __name__, url_prefix="/api/resource/external")


@external_resource_bp.route("/get", methods=["GET", "POST"])
@validate_request(schema={
    "resource_type": str
})
@requires_login()
@has_dynamic_permissions(PermissonLevel.USER, ["external_resource.{resource_type}.get"])
@mongo_query_validate()
def external_resource_get(data: dict, user: User):
    resource_type = data["resource_type"]
    permission = "external_resource.{resource_type}.get".format(resource_type=resource_type)

    @has_permissions([permission], register_func_data=False)
    def inner_func(data: dict, user: User):

        orchestrator = Orchestrator.get()

        resources = [v for (k,v) in orchestrator.get_resources_of_type(data["resource_type"]).items()]

        selected_resources, count = list_query(resources, data)

        return list_to_flask_response(selected_resources, count)

    return inner_func(data, user)
