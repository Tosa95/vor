import subprocess
from uuid import uuid4

import eventlet
import eventlet.green.subprocess
from abc import ABC, abstractmethod

import cv2
import numpy as np
from numpy.core.multiarray import ndarray

from db.resources.dataset import VideoDatasetTransform, ImageDatasetTransform, TextDatasetTransform
from general_utils.multimedia.ffmpeg_utils import ffmpeg_limit_video_quality
from rest_api.resources.data import ResourceDataManager
from settings.special_paths import STUB_PROJECT_ID, STUB_RESOURCE_ID, RESOURCE_TEMP_PATH


class DatasetElementTransformer(ABC):

    @abstractmethod
    def transform(self, element_data):
        pass

    @abstractmethod
    def get_preview_suffix(self):
        pass

    @abstractmethod
    def load_from_document(self, data) -> "DatasetElementTransformer":
        pass


class ImageDatasetElementTransformer(DatasetElementTransformer):

    def __init__(self, max_size: int = None):
        self._max_size = max_size

    def transform(self, element_data: bytes):

        if not isinstance(element_data, ndarray):
            element_data = np.frombuffer(element_data, dtype="uint8")
        frame: ndarray = cv2.imdecode(element_data, 1)

        h, w = frame.shape[:2]

        resize_factor = self._max_size / max(h, w)

        target_w = int(w * resize_factor)
        target_h = int(h * resize_factor)

        target_size = (target_w, target_h)

        if target_w >= w or target_h >= h:
            return element_data

        zoom_factor_x = resize_factor
        zoom_factor_y = resize_factor

        scale_matrix = np.array([
            [zoom_factor_x, 0, 0],
            [0, zoom_factor_y, 0]
        ], dtype="float32")

        frame = cv2.warpAffine(frame, scale_matrix, target_size)

        encode_params = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
        result, frame = cv2.imencode('.jpg', frame, encode_params)

        return frame.tobytes()

    def get_preview_suffix(self):
        return "_resized_%d" % self._max_size

    def load_from_document(self, data: ImageDatasetTransform) -> "DatasetElementTransformer":
        return ImageDatasetElementTransformer(max_size=data.max_size)


class TextDatasetElementTransformer(DatasetElementTransformer):

    def __init__(self, max_length: int = None):
        self._max_length = max_length

    def transform(self, element_data: bytes):

        element_data = element_data.decode('utf-8')

        if len(element_data) <= self._max_length:
            return element_data.encode('utf-8')
        else:
            return (element_data[:self._max_length] + " ...").encode('utf-8')

    def get_preview_suffix(self):
        return "_truncated_%d" % self._max_length

    def load_from_document(self, data: TextDatasetTransform) -> "DatasetElementTransformer":

        return TextDatasetElementTransformer(max_length=data.max_length)


class VideoDatasetElementTransformer(DatasetElementTransformer):

    def __init__(self, average_bitrate=200000, result_width=320, start=-1, end=-1, keep_audio=True,
                 output_format="mp4", video_codec="libx264"):
        self._average_bitrate = average_bitrate
        self._result_width = result_width
        self._start = start
        self._end = end
        self._keep_audio = keep_audio
        self._output_format = output_format
        self._video_codec = video_codec

    def transform(self, element_data: bytes):
        # sudo ffmpeg -i cf75a389-4014-4b9b-bbc6-f1afd0f8e56c -c:v h264_omx -an prova.mp4
        rdm = ResourceDataManager.get()
        pid = STUB_PROJECT_ID
        rid = STUB_RESOURCE_ID
        path = RESOURCE_TEMP_PATH
        name = str(uuid4())
        output_name = name + "_reduced_quality"
        rdm.add_data(pid, rid, path, name, element_data)
        url = rdm.get_url(pid, rid, path, name)
        output_url = rdm.get_url(pid, rid, path, output_name)
        ffmpeg_limit_video_quality(url, output_url,
                                   average_bitrate=self._average_bitrate,
                                   # video_codec='h264_omx',
                                   video_codec=self._video_codec,
                                   result_width=self._result_width,
                                   start=self._start,
                                   end=self._end,
                                   keep_audio=self._keep_audio,
                                   subprocess_version=eventlet.green.subprocess,
                                   output_format=self._output_format)

        result = rdm.get_data(pid, rid, path, output_name)

        rdm.remove_data(pid, rid, path, output_name)
        rdm.remove_data(pid, rid, path, name)

        return result

    def get_preview_suffix(self):
        return f"_video_preview_{self._average_bitrate}_{self._video_codec}_{self._result_width}" \
               f"_{self._start}_{self._end}_{self._keep_audio}_{self._output_format}"

    def load_from_document(self, data: VideoDatasetTransform):
        return VideoDatasetElementTransformer(
            average_bitrate=data.average_bitrate,
            result_width=data.result_width,
            start=data.start,
            end=data.end,
            keep_audio=data.keep_audio,
            output_format=self._output_format,
            video_codec=self._video_codec
        )
