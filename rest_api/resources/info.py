from flask import Blueprint, jsonify
from schema import Optional

from db.projects import ProjectCollaborator
from db.resources.base import Resource, ResourceCollaborator, ResourceCollaboratorStatus
from db.users import User, has_permission
from rest_api.resources.data import ResourceDataManager
from settings.resources import ResourceInfo, RESOURCES
from common.decorators.decorators import validate_request, mongo_query_validate
from common.decorators.requirements import requires_login, requires_project, requires_resource
from common.decorators.permissions import has_permissions_on_resource, has_dynamic_permissions, PermissonLevel, \
    has_permissions_on_project
from common.errors import exception_json, ErrorID
from common.mongo import list_query, list_to_flask_response, mongo_query_result_to_json

# TODO: uniform id and user_id in the various calls...

resinfo_bp = Blueprint("res_info", __name__, url_prefix="/api/resource/info")


def resource_info_to_dict(res_info: ResourceInfo):
    return {
        "name": res_info.get_name(),
        "displayed_name": res_info.get_displayed_name()
    }


def resource_collaborator_to_resource(resource_collaborator: ResourceCollaborator):
    permissions = resource_collaborator.permissions
    resource = resource_collaborator.resource

    res = {
        "permissions": permissions
    }

    res.update(mongo_query_result_to_json(resource))

    return res


def resource_collaborator_to_user(resource_collaborator: ResourceCollaborator):
    permissions = resource_collaborator.permissions
    status = resource_collaborator.status
    user = resource_collaborator.user

    res = mongo_query_result_to_json(user)

    res.update({
        "permissions": permissions,
        "status": status
    })

    return res

@resinfo_bp.route("/self/addable/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
@requires_project()
@has_dynamic_permissions(PermissonLevel.PROJECT, ["resource.{resource_type}.add"])
@mongo_query_validate()
def info_resource_self_addable_get(data: dict, user: User, project_collaborator: ProjectCollaborator):
    res_infos = [resource_info_to_dict(res_info) for res_info in RESOURCES.values() if
                 has_permission(project_collaborator.permissions, "resource.%s.add" % res_info.get_name())]

    selected_infos, count = list_query(res_infos, data)

    return list_to_flask_response(selected_infos, count)


@resinfo_bp.route("/self/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
@requires_project()
@mongo_query_validate()
def info_resource_self_get(data: dict, user: User, project_collaborator: ProjectCollaborator):
    resource_collaborators = list(ResourceCollaborator.objects(user=user.id, project=project_collaborator.project.id))

    # Checking for not collaborating resources that user should see anyway
    collaborating_resources_ids = set()
    for collab in resource_collaborators:
        collaborating_resources_ids.add(collab.resource.id)

    project_resources = Resource.objects(project=project_collaborator.project.id)

    for res in project_resources:
        print(res.name)
        print(project_collaborator.permissions)
        if res.id not in collaborating_resources_ids and has_permission(project_collaborator.permissions,
                                                                        "resource.%s.get" % res.restype):
            resource_collaborators.append(
                ResourceCollaborator(user=user, project=project_collaborator.project.id, resource=res.id))

    for collab in resource_collaborators:
        collab.inherit_permissions_from_project(project_collaborator)

    resource_collaborators = [resource_collaborator_to_resource(resource_collaborator) for resource_collaborator in
                              resource_collaborators]

    selected, count = list_query(resource_collaborators, data)

    return list_to_flask_response(selected, count)


@resinfo_bp.route("/self/del", methods=["DELETE"])
@validate_request()
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.del"])
def info_resource_self_del(data: dict, user: User, project_collaborator: ProjectCollaborator,
                  resource_collaborator: ResourceCollaborator):
    try:
        resource_collaborator.resource.delete()
        ResourceDataManager.get().remove_path(project_collaborator.project.id, resource_collaborator.resource.id, "")
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Resource deleted."}), 200


@resinfo_bp.route("/collaborator/get", methods=["GET", "POST"])
@validate_request()
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.collaborator.get"])
@mongo_query_validate(force_exclude=["password_hash", "password_id"])
def info_resource_collaborator_get(data: dict, user: User, project_collaborator: ProjectCollaborator,
                              resource_collaborator: ResourceCollaborator):
    resource_collaborators = ResourceCollaborator.objects(project=project_collaborator.project.id,
                                                          resource=resource_collaborator.resource.id)

    for collab in resource_collaborators:
        collab.inherit_permissions_from_project()

    collaborators = [resource_collaborator_to_user(rc) for rc in resource_collaborators]

    selected_collaborators, count = list_query(collaborators, data)

    return list_to_flask_response(selected_collaborators, count)


@resinfo_bp.route("/collaborator/add", methods=["POST"])
@validate_request(schema={
    "user_id": str,
    Optional("permissions"): dict,
    Optional("status"): int
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.collaborator.add"])
def info_resource_collaborator_add(data: dict, user: User, project_collaborator: ProjectCollaborator,
                              resource_collaborator: ResourceCollaborator):
    user_id = data["user_id"]
    permissions = data.get("permissions", None)
    status = data.get("status", ResourceCollaboratorStatus.ACTIVE)

    new_collaborator = ResourceCollaborator(user=user_id, project=project_collaborator.project.id,
                                            resource=resource_collaborator.resource.id,
                                            permissions=permissions, status=status)

    try:
        new_collaborator.save()
    except Exception as e:
        return exception_json(e)

    return jsonify({"msg": "Collaborator added."}), 200


@resinfo_bp.route("/collaborator/del", methods=["DELETE"])
@validate_request(schema={
    "id": str
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.collaborator.del"])
def info_resource_collaborator_del(data: dict, user: User, project_collaborator: ProjectCollaborator,
                              resource_collaborator: ResourceCollaborator):
    collaborator_to_remove = ResourceCollaborator.objects(user=data["id"],
                                                          project=project_collaborator.project.id,
                                                          resource=resource_collaborator.resource.id).first()

    if collaborator_to_remove is None:
        return exception_json(Exception("Collaborator not found"), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_remove.delete()

    return jsonify({"msg": "Collaborator deleted."}), 200


@resinfo_bp.route("/collaborator/permission/add", methods=["POST"])
@validate_request(schema={
    "id": str,
    "permissions": list
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.collaborator.permission.set"])
def info_resource_collaborator_permission_add(data: dict, user: User, project_collaborator: ProjectCollaborator,
                                         resource_collaborator: ResourceCollaborator):
    collaborator_to_update = ResourceCollaborator.objects(user=data["id"],
                                                          project=project_collaborator.project.id,
                                                          resource=resource_collaborator.resource.id).first()

    if collaborator_to_update is None:
        return exception_json(Exception("Collaborator not found."), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_update.add_permissions(data["permissions"])
    collaborator_to_update.save()

    return jsonify({"msg": "Collaborator permissions added."}), 200


@resinfo_bp.route("/collaborator/permission/del", methods=["DELETE"])
@validate_request(schema={
    "id": str,
    "permissions": list
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.collaborator.permission.set"])
def info_resource_collaborator_permission_del(data: dict, user: User, project_collaborator: ProjectCollaborator,
                                         resource_collaborator: ResourceCollaborator):
    collaborator_to_update = ResourceCollaborator.objects(user=data["id"],
                                                          project=project_collaborator.project.id,
                                                          resource=resource_collaborator.resource.id).first()

    if collaborator_to_update is None:
        return exception_json(Exception("Collaborator not found"), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_update.remove_permissions(data["permissions"])
    collaborator_to_update.save()

    return jsonify({"msg": "Collaborator permissions removed."}), 200


@resinfo_bp.route("/collaborator/status/set", methods=["PATCH"])
@validate_request(schema={
    "id": str,
    "new_status": int
})
@requires_login()
@requires_project()
@requires_resource()
@has_permissions_on_resource(["resource.collaborator.status.set"])
def info_resource_collaborator_status_set(data: dict, user: User, project_collaborator: ProjectCollaborator,
                                     resource_collaborator: ResourceCollaborator):
    collaborator_to_update = ResourceCollaborator.objects(user=data["id"],
                                                          project=project_collaborator.project.id,
                                                          resource=resource_collaborator.resource.id).first()

    if collaborator_to_update is None:
        return exception_json(Exception("Collaborator not found."), 404, ErrorID.COLLABORATOR_NOT_FOUND)

    collaborator_to_update.status = data["new_status"]
    collaborator_to_update.save()

    return jsonify({"msg": "Collaborator status set."}), 200
