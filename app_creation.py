import os
from flask import Flask
from flask_socketio import SocketIO

from common.token.default_token_manager import TokenMode, DefaultTokenManager
from common.token.hmac_token_manager import HMACTokenManager
from common.token.rsa_token_manager import RSAKeyPair, RSATokenManager
from rest_api.documentation import documentation_bp
from rest_api.project import project_bp
from rest_api.resources.data import resdata_bp
from rest_api.resources.dataset import dataset_bp
from rest_api.resources.external import external_resource_bp
from rest_api.resources.info import resinfo_bp
from settings.resources import RESOURCES

DEFAULT_SECRET_KEY = "Thk2uPSx.Ny9FYKàHlh44d+WXWKCg3e1vaR4Ip2pMCBdgY14F6t28KPS32QgghlOQR6YUa!wèuJk1K4Wj53BV?C.v37qC0sHu?md"


def create_app():
    app = Flask(__name__)

    secret_key = os.environ.get("SECRET_KEY", None)
    token_mode = os.environ.get("TOKEN_MODE", TokenMode.HMAC)
    token_private_key_file = os.environ.get("TOKEN_PRIVATE_KEY_FILE", None)
    token_public_key_file = os.environ.get("TOKEN_PUBLIC_KEY_FILE", None)
    token_key_auto_generate = os.environ.get("TOKEN_KEY_AUTO_GENERATE", True)
    token_key_size = os.environ.get("TOKEN_KEY_SIZE", 4096)

    print("Token mode: %s" % token_mode)

    if secret_key is None and token_mode == TokenMode.HMAC:
        print(
            "WARNING: using default secret key. This is a security issue. "
            "You should set the secret key via the environment variable SECRET_KEY.")
        secret_key = DEFAULT_SECRET_KEY

    if token_mode == TokenMode.HMAC:
        DefaultTokenManager.set(HMACTokenManager(secret_key))
    elif token_mode == TokenMode.RSA:
        key_pair = RSAKeyPair.load(token_public_key_file, token_private_key_file, token_key_auto_generate,
                                   token_key_size)
        if key_pair is None:
            raise Exception("Could not load key pair")
        DefaultTokenManager.set(RSATokenManager(key_pair.public_key, key_pair.private_key))

    app.config["SECRET_KEY"] = secret_key
    app.config["SERVICE_ACCOUNT_PASSWORD_LENGTH"] = 256

    app.config["LOGIN_TOKEN_EXPIRY_SECONDS"] = 60 * 60 * 24
    # app.config["LOGIN_TOKEN_EXPIRY_SECONDS"] = 6

    app.config["RESET_PASSWORD_BASE_URL"] = "http://localhost:8080/app/#/reset_password/"

    mail_settings = {
        "MAIL_SERVER": 'smtp.gmail.com',
        "MAIL_PORT": 465,
        "MAIL_USE_TLS": False,
        "MAIL_USE_SSL": True,
        "MAIL_USERNAME": "vormailbot@gmail.com",
        "MAIL_PASSWORD": "vorrei32!"
    }

    app.config.update(mail_settings)

    socketio = SocketIO(app, cors_allowed_origins='*')

    # Registering the main blueprint to the app
    # import here to avoid circular imports
    from rest_api.user import user_bp
    from website_serving.vue_app import vue_app_bp
    app.register_blueprint(user_bp)
    app.register_blueprint(vue_app_bp)
    app.register_blueprint(project_bp)
    app.register_blueprint(resinfo_bp)
    app.register_blueprint(resdata_bp)
    app.register_blueprint(documentation_bp)
    app.register_blueprint(external_resource_bp)

    for res in RESOURCES.values():
        app.register_blueprint(res.get_blueprint())

        if res.get_websocket_namespace() is not None:
            socketio.on_namespace(res.get_websocket_namespace()("/resource/%s" % res.get_name()))

    return app, socketio
