import re
import threading
from queue import Queue, Full, Empty
from threading import Thread, RLock
import time
from marlin_connector.marlin_commands import BaseMarlinCommand
# import eventlet
from abc import ABC, abstractmethod
import serial
from serial.serialutil import SerialException


#
# serial = eventlet.import_patched('serial')
# SerialException = serial.serialutil.SerialException

class BaseMarlinLogger(ABC):

    @abstractmethod
    def on_command_issued(self, command):
        pass

    @abstractmethod
    def on_machine_output(self, output):
        pass

    @abstractmethod
    def on_command_completed(self):
        pass


class ConsoleMarlinLogger(BaseMarlinLogger):

    def __init__(self, log_machine_output=False):
        self._log_machine_output = log_machine_output

    def on_command_issued(self, command):
        print("Command issued: %s" % command)

    def on_machine_output(self, output):
        if self._log_machine_output:
            print("Machine: %s" % output)

    def on_command_completed(self):
        print("Command completed")


class StubMarlinLogger(BaseMarlinLogger):

    def on_command_issued(self, command):
        pass

    def on_machine_output(self, output):
        pass

    def on_command_completed(self):
        pass


class Position:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


class MarlinCommunicator:
    _instN = 0

    @classmethod
    def get_empty_status(cls):
        return {
            "position": [0, 0, 0],
            "commands_to_be_completed": 0,
            "error": False,
            "error_cause": None
        }

    def __init__(self, serial_path, baud, logger: BaseMarlinLogger = StubMarlinLogger(), timeout=0.01,
                 startup_wait_seconds=2, sleeper=time):
        self._logger = logger
        self._timeout = timeout
        self._serial_path = serial_path
        self._baud = baud
        self._commands_to_be_completed = 0
        self._clbks = {}
        self._queue = Queue()
        self._ser = None
        self.open_serial()
        self._mustBeAborted = False
        self._workingThread = Thread(name="ArduinoCommunicator_worker", target=self._write_cycle)
        self._readingThread = Thread(name="ArduinoCommunicator_worker_read", target=self._read_cycle)
        self._workingThread.start()
        self._readingThread.start()
        self._cmds_to_be_completed_lock = RLock()
        self._instNCurr = self._instN
        self._position = Position(None, None, None)
        self._status_changed_callback = None
        self._status = self.get_empty_status()
        self._sleeper = sleeper
        MarlinCommunicator._instN += 1
        time.sleep(startup_wait_seconds)
        print("INSTN: " + str(MarlinCommunicator._instN))

    def open_serial(self):
        self._ser = serial.Serial(
            port=self._serial_path,
            baudrate=self._baud,
            timeout=self._timeout,
            # xonxoff=False,
            # rtscts=False,
            # dsrdtr=False,
            exclusive=True
        )

    def get_status(self):
        return self._status

    def set_status_changed_callback(self, callback):
        self._status_changed_callback = callback

    def fire_status_changed(self):
        if self._status_changed_callback is not None:
            self._status_changed_callback(self._status)

    def set_position(self, x, y, z):
        self._status["position"] = [x, y, z]
        self.fire_status_changed()

    def set_error(self, error_cause: str):
        self._mustBeAborted = True

        try:
            self._ser.close()
        except Exception as e:
            # Trying to close the serial, if it doesn't work simply we can't do it
            pass

        self._status["error"] = True
        self._status["error_cause"] = error_cause
        self.fire_status_changed()

    def registerCallback(self, cmd, clbk):
        self._clbks[cmd] = clbk

    def unregisterCallback(self, cmd):
        self._clbks.pop(cmd)

    def _read_cycle(self):
        read = ""

        while not self._mustBeAborted:

            try:

                readstr = self._ser.read(100).decode()

                for readc in readstr:
                    if readc == '\n':
                        self._logger.on_machine_output(read)
                        if read == "ok":
                            self.dec_commands_to_be_completed()
                            self._logger.on_command_completed()
                            # print("TO BE COMPLETED: %d" % self._commands_to_be_completed)
                        else:
                            position_match = re.search(r"^X:(-?\d+\.\d+) Y:(-?\d+\.\d+) Z:(-?\d+\.\d+)", read)
                            if position_match:
                                self.set_position(
                                    float(position_match.group(1)),
                                    float(position_match.group(2)),
                                    float(position_match.group(3))
                                )
                        read = ""
                    else:
                        read = read + readc

                if len(readstr) == 0:
                    time.sleep(0.01)

            except Exception as e:
                self.set_error(str(e))

    def set_commands_to_be_completed(self, val):
        self._cmds_to_be_completed_lock.acquire()
        self._commands_to_be_completed = val
        # print("Residual commands: %d" % self._commands_to_be_completed)
        self._cmds_to_be_completed_lock.release()
        self._status["commands_to_be_completed"] = self._commands_to_be_completed
        self.fire_status_changed()

    def dec_commands_to_be_completed(self):
        self._cmds_to_be_completed_lock.acquire()
        self.set_commands_to_be_completed(max(0, self._commands_to_be_completed - 1))
        self._cmds_to_be_completed_lock.release()

    def inc_commands_to_be_completed(self, amt=1):
        self._cmds_to_be_completed_lock.acquire()
        self.set_commands_to_be_completed(self._commands_to_be_completed + amt)
        self._cmds_to_be_completed_lock.release()

    def _write_cycle(self):

        while not self._mustBeAborted:

            # print "In communicator cycle"

            try:

                command = self._queue.get(timeout=0.1)
                self._logger.on_command_issued(command.replace("\n", ""))
                self._ser.write(command.encode())
                self._ser.flush()

            except Empty as e:
                pass  # It's ok to have an empty queue
            except Exception as e:
                self.set_error(str(e))

    def send_command_string(self, command_string):
        self.inc_commands_to_be_completed()
        self._queue.put(command_string)

    def send_command(self, command: BaseMarlinCommand):
        for command_string in command.get_string_commands():
            self.send_command_string(command_string)

    def send_command_sync(self, command: BaseMarlinCommand, poll_time=0.1):
        for command_string in command.get_string_commands():
            self.send_command_string(command_string)
        self.wait_for_commands_completion(poll_time=poll_time)

    def stop(self):

        # TODO: timeout
        self._mustBeAborted = True
        self._workingThread.join()
        self._readingThread.join()
        self._ser.close()

    def get_commands_to_be_completed(self):
        return self._commands_to_be_completed

    def wait_for_commands_completion(self, poll_time=0.1):
        while self._commands_to_be_completed > 0:
            self._sleeper.sleep(poll_time)
