import math
from abc import ABC, abstractmethod

from marlin_connector.marlin_commands import LinearMove, GetPosition, AutoHome, SetPrintMaxAcceleration
from marlin_connector.marlin_communication import MarlinCommunicator
import numpy as np


class BaseWorkSpaceCheck(ABC):

    @abstractmethod
    def check(self, position):
        pass

    @abstractmethod
    def correct(self, position):
        pass


class DeltaWorkSpaceCheck(BaseWorkSpaceCheck):

    def __init__(self, max_radius, min_z, max_z):
        self._max_radius = max_radius
        self._min_z = min_z
        self._max_z = max_z

    def check(self, position):
        x, y, z = position

        if z < self._min_z or z > self._max_z:
            return False

        radius = math.sqrt(x ** 2 + y ** 2)

        if radius > self._max_radius:
            return False

        return True

    def correct(self, position):

        x, y, z = position

        if z > self._max_z:
            z = self._max_z

        if z < self._min_z:
            z = self._min_z

        radius = math.sqrt(x ** 2 + y ** 2)

        if radius > self._max_radius:
            plane_pos = np.array([x, y], dtype=np.float)
            plane_pos /= radius
            plane_pos *= self._max_radius
            x, y = plane_pos

        return [x, y, z]


class MarlinCommandManager:

    def __init__(self, marlin_communicator: MarlinCommunicator,
                 work_space_check: BaseWorkSpaceCheck = DeltaWorkSpaceCheck(50, 10, 250)):
        self._marlin_communicator = marlin_communicator
        marlin_communicator.set_status_changed_callback(self.on_marlin_status_changed)
        self.adapt_marlin_status(marlin_communicator.get_status())
        self._status_changed_callback = None
        self._work_space_check = work_space_check

    def set_on_status_changed_callback(self, callback):
        self._status_changed_callback = callback

    def get_status(self):
        return self._status

    def set_work_space_check(self, work_space_check):
        self._work_space_check = work_space_check

    def fire_on_status_changed(self):
        if self._status_changed_callback is not None:
            self._status_changed_callback(self.get_status())

    def adapt_marlin_status(self, status):
        self._status = status

    def on_marlin_status_changed(self, status):
        # print("HERE")
        self.adapt_marlin_status(status)
        self.fire_on_status_changed()

    def linear_absolute_move(self, x=None, y=None, z=None):
        x, y, z = self._work_space_check.correct([x, y, z])
        self._marlin_communicator.send_command_sync(LinearMove(x=x, y=y, z=z))
        self._marlin_communicator.send_command_sync(GetPosition())

    def linear_relative_move(self, dx=0, dy=0, dz=0):
        position = np.array(self._status["position"], dtype="float64")
        delta = np.array([dx, dy, dz], dtype="float64")

        position += delta

        x, y, z = self._work_space_check.correct(position)
        self._marlin_communicator.send_command_sync(LinearMove(x=x, y=y, z=z))
        self._marlin_communicator.send_command_sync(GetPosition())

    def auto_home(self):
        self._marlin_communicator.send_command_sync(AutoHome())
        self._marlin_communicator.send_command_sync(GetPosition())

        x, y, z = self._status["position"]

        self.linear_absolute_move(x, y, z)

    def set_max_acceleration(self, max_acceleration):
        self._marlin_communicator.send_command_sync(SetPrintMaxAcceleration(acc=max_acceleration))

    def set_speed(self, speed):
        self._marlin_communicator.send_command_sync(LinearMove(feed_rate=speed))
