from abc import ABC, abstractmethod


def build_command(cmd, **kwargs):
    args_formatted = " ".join(["".join([k, str(v)]) for k, v in kwargs.items()])
    return cmd + " " + args_formatted + "\n"


class BaseMarlinCommand(ABC):

    def __init__(self, name, force_sync=True, **kwargs):
        self._commands = [build_command(name, **kwargs)]
        if force_sync:
            self._commands.append(build_command("M400"))

    def get_string_commands(self):
        return self._commands


class AutoHome(BaseMarlinCommand):

    def __init__(self, **kwargs):
        super().__init__("G28", **kwargs)


class LinearMove(BaseMarlinCommand):

    def __init__(self, feed_rate=None, x=None, y=None, z=None, **kwargs):

        if feed_rate is not None:
            kwargs["F"] = feed_rate

        if x is not None:
            kwargs["X"] = float(x)

        if y is not None:
            kwargs["Y"] = float(y)

        if z is not None:
            kwargs["Z"] = float(z)

        super().__init__("G0", **kwargs)


class SetPrintMaxAcceleration(BaseMarlinCommand):

    def __init__(self, extruder_acc=None, target_extruder=None, acc=None, x_acc=None, y_acc=None, z_acc=None, **kwargs):

        if extruder_acc is not None:
            kwargs["E"] = extruder_acc

        if target_extruder is not None:
            kwargs["T"] = target_extruder

        if acc is not None:
            kwargs["X"] = acc
            kwargs["Y"] = acc
            kwargs["Z"] = acc

        if x_acc is not None:
            kwargs["X"] = x_acc

        if y_acc is not None:
            kwargs["Y"] = y_acc

        if z_acc is not None:
            kwargs["Z"] = z_acc

        super().__init__("M201", **kwargs)


class SetBedTemperature(BaseMarlinCommand):

    def __init__(self, temp=None, **kwargs):
        if temp is not None:
            kwargs["S"] = temp

        super().__init__("M190", **kwargs)


class GetPosition(BaseMarlinCommand):
    def __init__(self, **kwargs):
        super().__init__("M114", **kwargs)
