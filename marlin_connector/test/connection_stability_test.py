import random
import time
from pprint import pprint

from marlin_connector.marlin_command_manager import MarlinCommandManager, DeltaWorkSpaceCheck
from marlin_connector.marlin_communication import MarlinCommunicator

if __name__ == '__main__':
    communicator = MarlinCommunicator(
        "/dev/ttyUSB0",
        250000
    )

    manager = MarlinCommandManager(communicator, DeltaWorkSpaceCheck(
        10,
        100,
        200
    ))


    def on_marlin_status_changed(status):
        pprint(status)

    manager.set_on_status_changed_callback(on_marlin_status_changed)

    time.sleep(2)

    manager.set_speed(15000)
    manager.set_max_acceleration(600)

    manager.auto_home()

    for i in range(10000):
        x = random.randint(-15,15)
        y = random.randint(-15,15)
        z = random.randint(100,200)
        manager.linear_absolute_move(x, y, z)