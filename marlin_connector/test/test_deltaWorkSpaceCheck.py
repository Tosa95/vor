from unittest import TestCase

from marlin_connector.marlin_command_manager import DeltaWorkSpaceCheck


class TestDeltaWorkSpaceCheck(TestCase):

    def test_basic_usage(self):
        check = DeltaWorkSpaceCheck(60, 10, 270)

        self.assertTrue(check.check([10, 10, 100]))
        self.assertTrue(check.check([10, 10, 10]))
        self.assertTrue(check.check([60, 0, 10]))
        self.assertFalse(check.check([60.1, 0, 10]))
        self.assertFalse(check.check([50, 50, 10]))
        self.assertFalse(check.check([0, 70, 10]))
        self.assertFalse(check.check([0, 0, 271]))

        self.assertTrue(check.check(check.correct([70, 70, 100])))
        self.assertTrue(check.check(check.correct([70, 70, 500])))
        self.assertTrue(check.check(check.correct([70, 70, 0])))
