import logging

import eventlet

from settings.multimedia import MultimediaSettings

eventlet.monkey_patch()

from rest_api.resources.data import ResourceDataManager, FilesystemResourceDataManager

import redis

from common.orchestration.redis_direct_orchestrator import RedisDirectOrchestrator
from settings.orchestrator import Orchestrator

from mongoengine import connect

from app_creation import create_app

current_socketio = None

if __name__ == '__main__':

    from db.users import User, USER_TYPE_REAL
    import os

    redis_host = os.environ.get("REDIS_HOST", "localhost")
    redis_port = int(os.environ.get("REDIS_PORT", 6379))
    redis_db = int(os.environ.get("REDIS_DB", 0))
    r = redis.Redis(host=redis_host, port=redis_port, db=redis_db)
    orchestrator = RedisDirectOrchestrator(r, threaded=True)

    Orchestrator.set(orchestrator)

    mongo_host = os.environ.get("MONGO_HOST", "localhost")
    mongo_port = int(os.environ.get("MONGO_PORT", 27017))
    mongo_db = os.environ.get("MONGO_DB", "vor")

    mongo_username = os.environ.get("MONGO_USERNAME", None)
    mongo_password = os.environ.get("MONGO_PASSWORD", None)

    if mongo_username is None:
        connect(mongo_db, host=mongo_host, port=mongo_port)
    else:
        connect(mongo_db, host=mongo_host, port=mongo_port, username=mongo_username, password=mongo_password,
                authentication_source="admin")

    res_data_base_dir = os.environ.get("RESOURCE_DATA_BASE_DIR", None)
    ResourceDataManager.set(FilesystemResourceDataManager(res_data_base_dir))

    video_codec = os.environ.get("VIDEO_CODEC", "h264_omx")
    video_format = os.environ.get("VIDEO_FORMAT", "mp4")
    MultimediaSettings.set(MultimediaSettings(video_codec, video_format))

    # Creating superuser if no user is present
    if User.objects().count() == 0:
        try:
            vor = User(username="vor", email="vor@vor.vor", user_type=USER_TYPE_REAL)
            vor.hash_password("vor")
            vor.add_permissions(["*"])
            vor.save()
        except:
            pass

    app, socketio = create_app()
    current_socketio = socketio
    app.debug = True

    logging.getLogger('socketio').setLevel(logging.ERROR)
    logging.getLogger('engineio').setLevel(logging.ERROR)

    socketio.run(app, host='0.0.0.0')
