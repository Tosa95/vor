from mongoengine import *

connect("prova")

class User(Document):
    email = StringField(required=True, unique=True)
    first_name = StringField()
    last_name = StringField()

    def __repr__(self):
        return self.email


# User(email="aaaaaaa@gmail.com", first_name="Antonello", last_name="Giovannino").save()

print(User.objects(first_name="Antonello"))