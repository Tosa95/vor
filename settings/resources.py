from flask import Blueprint
from socketio import Namespace

from db.resources.base import ResourceNames
from rest_api.resources.dataset import dataset_bp
from rest_api.resources.deltascope_websocket import DeltascopeNamespace
from rest_api.resources.deltascope import deltascope_bp

class ResourceInfo:

    def __init__(self, name: str, displayed_name: str, blueprint: Blueprint, websocket_namespace: Namespace = None):
        self._name = name
        self._displayed_name = displayed_name
        self._blueprint = blueprint
        self._websocket_namespace = websocket_namespace

    def get_name(self):
        return self._name

    def get_displayed_name(self):
        return self._displayed_name

    def get_blueprint(self):
        return self._blueprint

    def get_websocket_namespace(self):
        return self._websocket_namespace


RESOURCES = {
    ResourceNames.DELTASCOPE: ResourceInfo(ResourceNames.DELTASCOPE, "DeltaScope", deltascope_bp, DeltascopeNamespace),
    ResourceNames.DATASET: ResourceInfo(ResourceNames.DATASET, "DataSet", dataset_bp, None)
}
