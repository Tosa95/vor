from common.orchestration.base_orchestrator import BaseOrchestrator


class Orchestrator:
    instance = None

    @classmethod
    def set(cls, orchestrator: BaseOrchestrator):
        cls.instance = orchestrator

    @classmethod
    def get(cls) -> BaseOrchestrator:
        return cls.instance