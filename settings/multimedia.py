class MultimediaSettings:

    instance = None

    def __init__(self, video_codec, video_format):
        self.video_codec = video_codec
        self.video_format = video_format

    @classmethod
    def get(cls) -> "MultimediaSettings":
        return cls.instance

    @classmethod
    def set(cls, instance):
        cls.instance = instance